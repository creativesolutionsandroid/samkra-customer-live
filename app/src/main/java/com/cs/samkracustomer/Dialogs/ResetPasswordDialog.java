package com.cs.samkracustomer.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
;

import com.cs.samkracustomer.Activity.ForgotPasswordActivity;
import com.cs.samkracustomer.Activity.SignInActivity;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextInputLayout inputLayoutPassword, inputLayoutConfirmPassword;
    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword, otp;
    Button buttonSubmit, buttonVerify;
    String strMobile;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String language;
    SharedPreferences LanguagePrefs;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    AlertDialog loaderDialog;
    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        }
        else {
            rootView = inflater.inflate(R.layout.dialog_reset_password_ar, container, false);
        }

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getArguments().getString("mobile");
        otp = getArguments().getString("otp");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputLayoutPassword = (TextInputLayout) rootView.findViewById(R.id.input_layout_password);
        inputLayoutConfirmPassword = (TextInputLayout) rootView.findViewById(R.id.input_layout_retype_password);
        inputPassword = (EditText) rootView.findViewById(R.id.reset_input_password);
        inputConfirmPassword = (EditText) rootView.findViewById(R.id.reset_input_retype_password);

        buttonSubmit = (Button) rootView.findViewById(R.id.reset_submit_button);

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
            inputPassword.setTypeface(Constants.getarTypeFace(getContext()));
            inputConfirmPassword.setTypeface(Constants.getarTypeFace(getContext()));


        }

        buttonSubmit.setOnClickListener(this);

        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setTypeface(){
//        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
//                "helvetica.ttf");

        inputPassword.setTypeface(Constants.getarTypeFace(getContext()));
        inputConfirmPassword.setTypeface(Constants.getarTypeFace(getContext()));
        buttonSubmit.setTypeface(Constants.getarTypeFace(getContext()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reset_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            } else {
                inputLayoutConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match_ar));
            }
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reset_input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.reset_input_retype_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Phone","966"+strMobile);
            parentObj.put("Password",strPassword);
            parentObj.put("Otp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.resetPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    if(response.isSuccessful()){
                        Signupresponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserid();
                                userPrefsEditor.putString("userId", userId);
                                userPrefsEditor.putString("name", resetPasswordResponse.getData().getUsername());
                                userPrefsEditor.putString("email", resetPasswordResponse.getData().getEmail());
                                userPrefsEditor.putString("mobile", resetPasswordResponse.getData().getPhone());
                                userPrefsEditor.putString("pic",resetPasswordResponse.getData().getProfilephoto());
                                userPrefsEditor.commit();
                                if (language.equalsIgnoreCase("En")){
                                    Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.reset_success_msg_ar, Toast.LENGTH_SHORT).show();
                                }

                                ForgotPasswordActivity.isResetSuccessful = true;
                                getDialog().dismiss();
                            }
                            else {
//                                status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
    public void showloaderAlertDialog(){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.loder_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
            loaderDialog = dialogBuilder.create();
            loaderDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = loaderDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
        }
    }


