package com.cs.samkracustomer.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Activity.SignInActivity;
import com.cs.samkracustomer.Activity.SplashScreen;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.samkracustomer.Activity.ForgotPasswordActivity.isResetSuccessful;

public class VerifyOtpDialog extends AppCompatActivity implements View.OnClickListener {

    String otpEntered = "", strName, strEmail, strNameAr, strMobile, strPassword;
    Button buttonVerify;
    ImageView back_btn;
    TextView buttonResend;
    //    ImageView imgEditMobile;
    String language;
    OtpView otpView;
    TextView textMobileNumber;
    ;
    private String screen = "";
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    AlertDialog loaderDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.vefiry_otp);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.vefiry_otp_ar);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        userPrefsEditor = userPrefs.edit();

        otpView = (OtpView) findViewById(R.id.otp_view);
        textMobileNumber = (TextView) findViewById(R.id.mobile);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        buttonVerify = (Button) findViewById(R.id.button_verify_otp);
        buttonResend = (TextView) findViewById(R.id.button_resend_otp);

        strName = getIntent().getStringExtra("name");
        strEmail = getIntent().getStringExtra("email");
        strMobile = getIntent().getStringExtra("mobile");
        strPassword = getIntent().getStringExtra("password");
        screen = getIntent().getStringExtra("screen");
        textMobileNumber.setText(Constants.Country_Code + strMobile);

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);
//        setTypeface();
        setTimerForResend();

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                otpEntered = otp;
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonVerify.setOnClickListener(this);
        buttonResend.setOnClickListener(this);
//        imgEditMobile.setOnClickListener(this);
        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
    }

    private void setTypeface() {
        ((TextView) findViewById(R.id.tv1)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.mobile)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.button_resend_otp)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
        ((TextView) findViewById(R.id.button_verify_otp)).setTypeface(Constants.getarTypeFace(VerifyOtpDialog.this));
    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
//                Log.i(TAG, "onTick: "+timeRemaining);
//                if(getDialog()!=null) {
                if (language.equalsIgnoreCase("En")) {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
                } else {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar) + " في " + timeRemaining);
                }
            }

            public void onFinish() {
//                if(getDialog()!=null) {
                if (language.equalsIgnoreCase("En")) {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                } else {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar));
                }

                buttonResend.setEnabled(true);
                buttonResend.setAlpha(1.0f);
//                }
            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_verify_otp:
                if (otpEntered.length() != 4) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                                getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), VerifyOtpDialog.this);
                    } else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar),
                                getResources().getString(R.string.alert_invalid_otp_ar), getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                    }
                } else {
                    if (screen.equals("register")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new userRegistrationApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else if (screen.equals("forgot")) {
//                        Intent i = new Intent(VerifyOtpDialog.this,ResetPasswordDialog.class);
//                        i.putExtra("mobile", strMobile);
//                        i.putExtra("otp", otpEntered);
//                        startActivity(i);
                        displayResetPasswordDiaolg();
                    }
                }
                break;

//            case R.id.edit_mobile_number:
////                SignUpActivity.isOTPVerified = false;
//                getDialog().cancel();
//                break;

            case R.id.button_resend_otp:
                if (screen.equals("register")) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new verifyMobileApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (screen.equals("forgot")) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ForgotPasswordApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject mobileObj = new JSONObject();
        try {
//            mobileObj.put("MobileNo","966"+strMobile);
            mobileObj.put("Phone", "966" + strMobile);
            mobileObj.put("Email", strEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Username", strName);
            parentObj.put("Email", strEmail);
            parentObj.put("Phone", "966" + strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("Otp", otpEntered);
            parentObj.put("Language", language);
            parentObj.put("DeviceToken", SplashScreen.regId);

            Log.d(TAG, "prepareSignUpJson: " + parentObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private class userRegistrationApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.userRegistration(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getUsername());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilephoto());
                            userPrefsEditor.commit();
                            Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
                            intent.putExtra("type", "1");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
//                          status false case
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            } else {
                                String failureResponse = registrationResponse.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                            }
                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                Log.i(TAG, "otp: " + verifyMobileResponse.getData().getOtp());
                            } else {
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VerifyOtpDialog.this);
                                } else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

//    @Override
////    public void onBackPressed() {
////
////    }

    private void displayResetPasswordDiaolg() {
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", otpEntered);

        isResetSuccessful = false;

        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
        newFragment.setCancelable(true);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }

                if (isResetSuccessful) {
                    Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
                    intent.putExtra("type", "1");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOtpDialog.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private String prepareForgotPasswordJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("phone", "966" + strMobile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareForgotPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.forgotPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse forgotPasswordResponse = response.body();
                        try {
                            if (forgotPasswordResponse.getStatus()) {
                                Log.i(TAG, "onResponse: " + forgotPasswordResponse.getData().getOtp());
                            } else {
                                // status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = forgotPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VerifyOtpDialog.this);
                                } else {
                                    String failureResponse = forgotPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }

                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (loaderDialog != null) {
                loaderDialog.dismiss();
            }
        }
    }
}
