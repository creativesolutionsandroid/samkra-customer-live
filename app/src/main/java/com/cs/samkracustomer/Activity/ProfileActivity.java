package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Adapters.ReviewsAdapter;
import com.cs.samkracustomer.Models.ViewProfileResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    public Context context;
    ImageView backbtn;
    CircleImageView icon;
    int bidPos;
    ArrayList<ViewProfileResponce.Data> bidData = new ArrayList<>();

    ArrayList<ViewProfileResponce.Wr> reviews = new ArrayList<>();
    TextView wrokshopname, wrokshopdetails, custmore_reviews;
    RatingBar ratingBar;
    ListView requestsList;
    ReviewsAdapter mAdapter;
    ArrayList<ViewProfileResponce.Data> carData = new ArrayList<>();
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    AlertDialog loaderDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.view_profile);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.view_profile_ar);
        }
//        setContentView(R.layout.view_profile);

        wrokshopname = (TextView) findViewById(R.id.wr_workshopid);
        wrokshopdetails = (TextView) findViewById(R.id.wr_summerytex);
        custmore_reviews = (TextView) findViewById(R.id.wr_revies);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        backbtn = (ImageView) findViewById(R.id.pf_back);
        icon =(CircleImageView) findViewById(R.id.vp_icon);
        String networkStatus = NetworkUtil.getConnectivityStatusString(ProfileActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new  userLoginApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")){
                Toast.makeText(ProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(ProfileActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

        requestsList = (ListView) findViewById(R.id.listview_pf);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        ((TextView)findViewById(R.id.wr_workshopid)).setTypeface(Constants.getarTypeFace(ProfileActivity.this));
        ((TextView)findViewById(R.id.wr_summerytex)).setTypeface(Constants.getarTypeFace(ProfileActivity.this));
        ((TextView)findViewById(R.id.wr_revies)).setTypeface(Constants.getarTypeFace(ProfileActivity.this));
    }

    private class userLoginApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProfileActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ViewProfileResponce> call = apiService.ProfileRequst(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ViewProfileResponce>() {
                @Override
                public void onResponse(Call<ViewProfileResponce> call, Response<ViewProfileResponce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if (response.isSuccessful()) {
                        ViewProfileResponce registrationResponse = response.body();
                        try {
                            if (registrationResponse.getStatus()) {
                                //                          status true case
                                bidData.addAll(registrationResponse.getData());
                                reviews.addAll(registrationResponse.getData().get(0).getWr());

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = registrationResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ProfileActivity.this);
                                }
                                else {
                                    String failureResponse = registrationResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ProfileActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                    setText();
                }

                @Override
                public void onFailure(Call<ViewProfileResponce> call, Throwable t) {
                    Log.d("TAG", "onFailure: "+t.getMessage().toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ProfileActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareLoginJson() {
            JSONObject parentObj = new JSONObject();

            try {
                parentObj.put("WorkshopId", getIntent().getIntExtra("id",0));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareLoginJson: "+parentObj.toString());
            return parentObj.toString();
        }
    }

    private void setText() {
        if (language.equalsIgnoreCase("En")){
            wrokshopname.setText(bidData.get(0).getNameen());
            wrokshopdetails.setText(bidData.get(bidPos).getAboutEn());
        }
        else {
            wrokshopname.setText(bidData.get(0).getNamear());
            wrokshopdetails.setText(bidData.get(bidPos).getAboutAr());
        }
        if(language.equalsIgnoreCase("En")) {
            custmore_reviews.setText("(" + reviews.size() + " Reviews)");
        }
        else {
            custmore_reviews.setText("( آراء "+reviews.size()+")");
        }

        try {
            Glide.with(ProfileActivity.this)
                    .load(Constants.WORKSHOP_IMAGES+bidData.get(bidPos).getLogo())
                    .into(icon);
            ratingBar.setRating(bidData.get(bidPos).getWr().get(0).getRating());
            mAdapter = new ReviewsAdapter(ProfileActivity.this, reviews, "");
            requestsList.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
            ratingBar.setRating(5);
        }
    }

    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
