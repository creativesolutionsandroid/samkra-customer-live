package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Dialogs.VerifyOtpDialog;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class SignUpActivity extends AppCompatActivity {

    EditText inputPassword,inputName,inputEmail,inputMobile,inputConfirmpassword;
    TextView textRegister;
    Button button;
    ImageView back_btn;
    String strName, strEmail, strMobile, strPassword ,strConfirmpassword;
    public static boolean isOTPVerified = false;
    AlertDialog customDialog;

    public static final String[] SMS_RECIEVER = {
            android.Manifest.permission.RECEIVE_SMS
    };
    public static final int SMS_REQUEST = 1;
    private static final String TAG = "TAG";
    ImageView checkboxTnC;
    RelativeLayout tncLayout;
    TextView tncText;
    TextView st1;
    Boolean isTermsChecked = false;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    AlertDialog loaderDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_signup);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_signup_ar);
        }
        super.onCreate(savedInstanceState);

        checkboxTnC = (ImageView) findViewById(R.id.checkboxTnC);
        tncLayout = (RelativeLayout) findViewById(R.id.tncLayout);
        tncText = (TextView) findViewById(R.id.tNcText);
        inputName = (EditText) findViewById(R.id.username);
        inputEmail = (EditText) findViewById(R.id.email);
        inputMobile = (EditText) findViewById(R.id.phone);
        inputPassword = (EditText) findViewById(R.id.password);
        inputConfirmpassword = (EditText) findViewById(R.id.confirm);
        Button inputButton = (Button) findViewById(R.id.bt1);
        back_btn =(ImageView)findViewById(R.id.back_btn);
        st1=(TextView) findViewById(R.id.tv1);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        inputName.requestFocus();

        String desc = "<p>By signing up you agree to our <u>Terms and Conditions</u></p>";

        if (language.equalsIgnoreCase("En")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tncText.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tncText.setText(Html.fromHtml(desc));
            }
        }
        else {
            tncText.setText("لرجاء التسجيل و الموافقه على ");
        }

        checkboxTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTermsChecked){
                    isTermsChecked = true;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_selected));
                }
                else{
                    isTermsChecked = false;
                    checkboxTnC.setImageDrawable(getResources().getDrawable(R.drawable.catering_unselected));
                }
            }
        });

        tncText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutIntent = new Intent(SignUpActivity.this, WebViewActivity.class);
                if(language.equalsIgnoreCase("En")) {
                aboutIntent.putExtra("title", "Terms and Conditions");
                aboutIntent.putExtra("url", "http://workshop.samkra.com/about/termsandcontidions");
               }
                else{
                    aboutIntent.putExtra("title", "الأحكام والشروط");
                    aboutIntent.putExtra("url", "http://workshop.samkra.com/about/termsandcontidionsar");
                }
                startActivity(aboutIntent);
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        inputPassword.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        inputName.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        inputEmail.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        inputMobile.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        st1.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        tncText.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        inputConfirmpassword.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.tv1)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.username)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.email)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.phone)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.password)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.confirm)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        ((TextView)findViewById(R.id.bt1)).setTypeface(Constants.getarTypeFace(SignUpActivity.this));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt1:
                if (validations()) {
                    showtwoButtonsAlertDialog();
                }
                break;
        }
    }

    private void showtwoButtonsAlertDialog() {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView title = (TextView) dialogView.findViewById(R.id.title);
            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

            if (language.equalsIgnoreCase("En")){
                title.setText(R.string.opt_msg_verify);
                yes.setText(getResources().getString(R.string.ok));
                no.setText(getResources().getString(R.string.edit));
                desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1)+"+966 "+strMobile+getResources().getString(R.string.signup_alert_mobile_verify2));
            }
            else {
                title.setText(R.string.opt_msg_verify_ar);
                yes.setText(getResources().getString(R.string.ok_ar));
                no.setText(getResources().getString(R.string.edit_ar));
                desc.setText(getResources().getString(R.string.signup_alert_mobile_verify_ar)+"+966 "+strMobile+getResources().getString(R.string.signup_alert_mobile_verify2_ar));

            }

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
            desc.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
            yes.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
            no.setTypeface(Constants.getarTypeFace(SignUpActivity.this));
        }

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new verifyMobileApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    customDialog.dismiss();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
                    inputMobile.setSelection(inputMobile.length());
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

    private boolean validations() {
        strName = inputName.getText().toString().trim();
        strMobile = inputMobile.getText().toString().trim();
        strPassword = inputPassword.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        strConfirmpassword = inputConfirmpassword.getText().toString().trim();
        strMobile = strMobile.replace("+966 ","");
        if (strName.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            return false;
        } else if (strMobile.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }
            else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            return false;
        } else if (strMobile.length() != 9) {
            if (language.equalsIgnoreCase("En")){
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }
            else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
                }
            return false;
        }
        else if (strEmail.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            }
            else {
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }

            return false;
        }
        else if (!isValidEmail(strEmail)) {
            if (language.equalsIgnoreCase("En")){
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            }
            else {
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }

            return false;
        }
        else if (strPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }

            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")){
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));

            }
            else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                }

            return false;
        }
        else if (strConfirmpassword.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }
            else {
                inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            return false;
        }
        else if(!strPassword.equals(strConfirmpassword) ){
            if (language.equalsIgnoreCase("En")){
                inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_match_password));
            }
            else {
                inputConfirmpassword.setError(getResources().getString(R.string.signup_msg_enter_match_password_ar));
            }

            return false;
        }
        else if (!isTermsChecked){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_accept_terms),
                        getResources().getString(R.string.alert_terms), getResources().getString(R.string.ok), SignUpActivity.this);

            }
            else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_accept_terms_ar),
                        getResources().getString(R.string.alert_terms_ar), getResources().getString(R.string.ok_ar), SignUpActivity.this);
            }
            return false;
        }
        return true;
    }

    public boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: "+verifyMobileResponse.getData().getOtp());
                                showVerifyDialog();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), SignUpActivity.this);
                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), SignUpActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {

                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("Phone", "966" + strMobile);
                parentObj.put("Email", strEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
            return parentObj.toString();
        }

        private void showVerifyDialog() {
            Intent intent = new Intent(SignUpActivity.this, VerifyOtpDialog.class);
            intent.putExtra("name", strName);
            intent.putExtra("email", strEmail);
            intent.putExtra("mobile", strMobile);
            intent.putExtra("password", strPassword);
            intent.putExtra("screen", "register");
            startActivity(intent);
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
