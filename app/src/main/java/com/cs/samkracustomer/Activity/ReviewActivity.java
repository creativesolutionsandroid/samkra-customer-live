package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Dialogs.VerifyOtpDialog;
import com.cs.samkracustomer.Models.NotificationResponce;
import com.cs.samkracustomer.Models.ReviewResponse;
import com.cs.samkracustomer.Models.SaveuserRatingResponce;
import com.cs.samkracustomer.Models.VerifyMobileResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewActivity extends AppCompatActivity {

    final Context context = this;
    TextView workshopid, feedback,textcharactes,workshopname;
    RatingBar ratingbar;
    EditText suggestions;
    Button nothanks, submit;
    ArrayList<NotificationResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    AlertDialog loaderDialog;
    String str;
    float givenRating = 1;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ImageView backBtn;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.review);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.review_ar);
        }
        super.onCreate(savedInstanceState);


        workshopid = (TextView) findViewById(R.id.tv_shopid);
        feedback = (TextView) findViewById(R.id.tv_feedback);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        suggestions = (EditText) findViewById(R.id.ed_command);
        nothanks = (Button) findViewById(R.id.btn_nothanks);
        submit = (Button) findViewById(R.id.btn_submit);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        workshopname=(TextView)findViewById(R.id.workshopname);
        textcharactes =(TextView)findViewById(R.id.comment_chars);

        workshopid.setText(getIntent().getStringExtra("rCode"));
        ratingbar.setRating(getIntent().getFloatExtra("rating", 1));
        setComments(getIntent().getFloatExtra("rating", 1));
        workshopname.setText(getIntent().getStringExtra("wNm"));

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        suggestions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        suggestions.setRawInputType(InputType.TYPE_CLASS_TEXT);
        Log.d(TAG, "workshopname"+getIntent().getStringExtra("wNw"));

        suggestions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (language.equalsIgnoreCase("En")){
                    textcharactes.setText((300 - (text.length()))+" characters left");
                }
                else {
                    textcharactes.setText((300 - (text.length()))+"حرف متبقي");
                }

            }
        });
        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(b){
                    if(v == 0){
                        ratingBar.setRating(1);
                    }
                    setComments(v);
                }
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ReviewActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new RatingRequst().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        ((TextView)findViewById(R.id.comment_chars)).setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        ((TextView)findViewById(R.id.btn_nothanks)).setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        ((TextView)findViewById(R.id.btn_submit)).setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        ((TextView)findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        workshopid.setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        feedback.setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        workshopname.setTypeface(Constants.getarTypeFace(ReviewActivity.this));
        suggestions.setTypeface(Constants.getarTypeFace(ReviewActivity.this));
    }

    private void setComments(float rating){
        givenRating = rating;
        if(rating == 1){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("Terrible");
            }
            else {
                feedback.setText("سيئ جدا");
            }

        }
        else if(rating == 2){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("Bad");
            }
            else {
                feedback.setText("سيئ");
            }

        }
        else if(rating == 3){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("Average");
            }
            else {
                feedback.setText("متوسط");
            }

        }
        else if(rating == 4){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("Good");
            }
            else {
                feedback.setText("جيد جدا");
            }

        }
        else if(rating == 5){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("Excellent");
            }
            else {
                feedback.setText("ممتاز");
            }

        }

    }


    private boolean validations() {
        str = suggestions.getText().toString();
//        if (str.length() == 0) {
//            if (language.equalsIgnoreCase("En")){
//                suggestions.setError(getResources().getString(R.string.enter_suggestion));
//            }
//            else {
//                suggestions.setError(getResources().getString(R.string.enter_suggestion_ar));
//            }

//            return false;
//        }
        return true;
    }

    private class RatingRequst extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ReviewActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ReviewResponse> call = apiService.RatingRequst(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ReviewResponse>() {
                @Override
                public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                    if (response.isSuccessful()) {
                        ReviewResponse SaveuserRatingResponce = response.body();
                        try {
                            if (SaveuserRatingResponce.getStatus()) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = SaveuserRatingResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ReviewActivity.this);
                                }
                                else {
                                    String failureResponse = SaveuserRatingResponce.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ReviewActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ReviewResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ReviewActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ReviewActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ReviewActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WorkshopId", getIntent().getIntExtra("wId", 0));
            parentObj.put("RequestId", getIntent().getIntExtra("rId", 0));
            parentObj.put("UserName", userPrefs.getString("name", null));
            parentObj.put("Rating", givenRating);
            parentObj.put("Comments", str);
            parentObj.put("UserId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReviewActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}