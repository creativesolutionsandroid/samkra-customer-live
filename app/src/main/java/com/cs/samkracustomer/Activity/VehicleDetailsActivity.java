package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Carmakersmodelresponse;
import com.cs.samkracustomer.Models.SaveUserRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.GPSTracker;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleDetailsActivity extends AppCompatActivity {
    private static final int PLACE_PICKER_REQUEST = 1;
    final Context context = this;
    private Button request_btn;
    String str;
    String strcommandview, strlocationview;
    EditText commandview;
    Spinner carlist,yearlist,typelist;
    TextView maketext1,typetex1,yeartext1,hondavx,yearview,summerytex,textCharacters ;
    ImageView locationview,back_btn;
    String leftImages, rightImages, backImages, frontImages;
    ImageView imageView;
    Bitmap logo;
    TextView locationedittex;
    LinearLayout locationlayout;
    AlertDialog loaderDialog;

    private ArrayAdapter<String> cityAdapter, typeAdapter,yearAdapter;
    String strYear = "", strComment = "No Comments", strAddress;
    int makerId = 0, modelId =  0;
    int modelSelected = 0, classSelected = 0;
    Double latitude, longitude;

    ArrayList<Carmakersmodelresponse.Data> data = new ArrayList<>();
    ArrayList<String> cityArray = new ArrayList<>();
    ArrayList<String> typeArray = new ArrayList<>();
//    String[] years = {"","2018","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000",};

    ArrayList<String> years = new ArrayList<>();
    String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 3;
    private FusedLocationProviderClient mFusedLocationClient;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_vehicle_info);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_vehicle_info_ar);
        }
        super.onCreate(savedInstanceState);


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        leftImages = getIntent().getStringExtra("left");
        rightImages = getIntent().getStringExtra("right");
        backImages = getIntent().getStringExtra("back");
        frontImages = getIntent().getStringExtra("front");
        logo = VehicleImagesActivity.firstImage;

        imageView = (ImageView) findViewById(R.id.image);
        maketext1 = (TextView) findViewById(R.id.maketext1);
        yeartext1 = (TextView) findViewById(R.id.yeartex1);
        typetex1 = (TextView) findViewById(R.id.typetxt1);
        hondavx = (TextView) findViewById(R.id.hondavx);
        yearview = (TextView) findViewById(R.id.yearview);
        summerytex = (TextView) findViewById(R.id.summerytex);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        locationview = (ImageView) findViewById(R.id.locationview);
        locationedittex = (TextView) findViewById(R.id.locationeditex);
        commandview = (EditText) findViewById(R.id.commadview);
        request_btn = (Button) findViewById(R.id.Request_btn);
        locationlayout=(LinearLayout) findViewById(R.id.locationl);
        textCharacters=(TextView) findViewById(R.id.comment_chars);

        carlist = (Spinner) findViewById(R.id.carslist);
        yearlist = (Spinner) findViewById(R.id.yearlist);
        typelist = (Spinner) findViewById(R.id.typelist);

        imageView.setImageBitmap(logo);
        commandview.setImeOptions(EditorInfo.IME_ACTION_DONE);
        commandview.setRawInputType(InputType.TYPE_CLASS_TEXT);

//        prepareYears();

        commandview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (language.equalsIgnoreCase("En")){
                    textCharacters.setText((300 - (text.length()))+" characters left");
                }
                else {
                    textCharacters.setText((300 - (text.length()))+"حرف متبقي");
                }
            }
        });

        request_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleDetailsActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new SaveUserRequestApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleDetailsActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new CarmakersmodelApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }


        locationlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(VehicleDetailsActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        commandview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                summerytex.setText(s.toString());
                if (s.toString().length() > 0) {
                    strComment = s.toString();
                }
                else {
                    strComment = "No Comments";
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(VehicleDetailsActivity.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(VehicleDetailsActivity.this);
                try {
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(VehicleDetailsActivity.this);
            try {
                getGPSCoordinates();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        maketext1.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        locationedittex.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        commandview.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        yeartext1.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        typetex1.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        hondavx.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        yearview.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        summerytex.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        textCharacters.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        request_btn.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        ((TextView)findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        ((TextView)findViewById(R.id.maketxt)).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        ((TextView)findViewById(R.id.typetxt)).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
        ((TextView)findViewById(R.id.yeartxt)).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));

    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {

                    mFusedLocationClient.getLastLocation().addOnFailureListener(VehicleDetailsActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(VehicleDetailsActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String address = getAddress(latitude, longitude);
                locationedittex.setText(address);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(VehicleDetailsActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(VehicleDetailsActivity.this, "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(VehicleDetailsActivity.this, perm));
    }
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {



        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }


            if ("".equals(place.getAddress())) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(VehicleDetailsActivity.this);

//                    if(language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("Samkra");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Sorry! we couldn't detect your location. Please place the pin on your exact location.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
//                Toast.makeText(AddressActivity.this, "Please select a address", Toast.LENGTH_SHORT).show();
            } else {
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                try {
                    locationedittex.setText(place.getAddress());
                    strAddress = place.getAddress().toString();
//                    getAddress(latitude, longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                    locationedittex.setText(place.getAddress());
                    strAddress = place.getAddress().toString();
                }
            }
        }

    }

    private boolean validations(){
//        if (strComment.length() == 0){
//            if (language.equalsIgnoreCase("En")){
//                commandview.setError(getResources().getString(R.string.enter_command));
//            }
//            else {
//                commandview.setError(getResources().getString(R.string.enter_command_ar));
//            }
//
//            Constants.requestEditTextFocus(commandview, VehicleDetailsActivity.this);
//            return false;
//        }
//        else
        if(classSelected == 0 || modelSelected == 0 || strYear.equals("")){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog("Please select car details", getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), VehicleDetailsActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog("يرجى اختيار تفاصيل السيارة", getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok), VehicleDetailsActivity.this);
            }

            return false;
        }
        else if (strAddress.equals("")){
            if (language.equalsIgnoreCase("En")){
                Constants.showOneButtonAlertDialog("Please select address", getResources().getString(R.string.error),
                        getResources().getString(R.string.ok), VehicleDetailsActivity.this);
            }
            else {
                Constants.showOneButtonAlertDialog("يرجى اختيار العنوان", getResources().getString(R.string.error_ar),
                        getResources().getString(R.string.ok), VehicleDetailsActivity.this);
            }

            return false;

        }

        return true;
    }

//    private void prepareYears(){
//        Calendar calendar = Calendar.getInstance();
//        int todayYear = calendar.get(Calendar.YEAR);
//        for (int i = 0; i <= 15; i++){
//            years.add(""+(todayYear-i));
//        }
//    }

    private class CarmakersmodelApi extends AsyncTask<String, String, String> {


        String inputStr;
        String TAG = "TAG";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleDetailsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Carmakersmodelresponse> call = apiService.Carmakers();
            call.enqueue(new Callback<Carmakersmodelresponse>() {
                @Override
                public void onResponse(Call<Carmakersmodelresponse> call, Response<Carmakersmodelresponse> response) {
                    if (response.isSuccessful()) {
                        data = response.body().getData();
                        setSpinner();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Carmakersmodelresponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void setSpinner(){
        cityArray.add("");
        for (int i = 0; i < data.size(); i++){
            if (language.equalsIgnoreCase("En")){
                cityArray.add(data.get(i).getCarmakernameEn());
            }
            else {
                cityArray.add(data.get(i).getCarmakernameAr());
            }
            Log.i(TAG, "cityArray: "+cityArray.size());
        }

        modelSelected = 0;
        typeArray.add("");
        for (int j = 0; j < data.get(modelSelected).getMdl().size(); j++) {
            if (language.equalsIgnoreCase("En")){
                typeArray.add(data.get(modelSelected).getMdl().get(j).getModelnameEn());
            }
            {
                typeArray.add(data.get(modelSelected).getMdl().get(j).getModelnameAr());
            }
            Log.i(TAG, "typeArray: "+typeArray.size());
        }

        years.clear();
        years.add("");
        Calendar calendar = Calendar.getInstance();
        int todayYear = calendar.get(Calendar.YEAR);
        int startYear = Integer.parseInt(data.get(modelSelected).getMdl().get(0).getModelyear());
        for (int i = 0; i <= (todayYear - startYear); i++){
            years.add(""+(todayYear-i));
        }

        cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, cityArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
                }
                return v;
            }
        };
        typeAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner,typeArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
                }

                return v;
            }
        };
        yearAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, years) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.CENTER_HORIZONTAL);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                if (language.equalsIgnoreCase("Ar")) {
                    ((TextView) v).setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
                }
                return v;
            }
        };
        carlist.setAdapter(cityAdapter);
        typelist.setAdapter(typeAdapter);
        yearlist.setAdapter(yearAdapter);

        carlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maketext1.setText(cityArray.get(i));
                if(cityArray.get(i).equalsIgnoreCase("")) {
                    modelSelected = i;
                    makerId = data.get(i).getCarmakerid();
                    typeArray.clear();
                    typetex1.setText("");
                    hondavx.setText("");
                }
                else{
                    typetex1.setText("");
                    hondavx.setText("");
                    modelSelected = i;
                    makerId = data.get(i-1).getCarmakerid();
                    typeArray.clear();
                    typeArray.add("");
                    for (int j = 0; j < data.get((modelSelected-1)).getMdl().size(); j++) {
                        if (language.equalsIgnoreCase("En")){
                            typeArray.add(data.get((modelSelected-1)).getMdl().get(j).getModelnameEn());
                        }
                        else {
                            typeArray.add(data.get((modelSelected-1)).getMdl().get(j).getModelnameAr());
                        }

                    }
                }
                typelist.setAdapter(typeAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        typelist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(typeArray.get(i).equalsIgnoreCase("")){
                    years.clear();
                }
                else {
                    classSelected = i;
                    modelId = data.get((modelSelected-1)).getMdl().get((i - 1)).getModelid();
                    typetex1.setText(typeArray.get((i)));
                    hondavx.setText(typeArray.get((i)));

                    years.clear();
                    years.add("");
                    Calendar calendar = Calendar.getInstance();
                    int todayYear = calendar.get(Calendar.YEAR);
                    int startYear = Integer.parseInt(data.get((modelSelected-1)).getMdl().get((i - 1)).getModelyear());
                    for (int j = 0; j <= (todayYear - startYear); j++){
                        years.add(""+(todayYear-j));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        yearlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected: "+years.get(i));
                yeartext1.setText(years.get(i));
                if (language.equalsIgnoreCase("En")){
                    yearview.setText(years.get(i)+" Model");
                }
                else {
                    yearview.setText(years.get(i)+" نوع السيارة");
                }

                strYear = ""+years.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d(TAG, "onItemSelected: ");
            }
        });
    }

    private String prepareJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userId);
            parentObj.put("MakerId", makerId);
            parentObj.put("ModelId", modelId);
            parentObj.put("Year", strYear);
            parentObj.put("Address", strAddress);
            parentObj.put("Latitude", latitude);
            parentObj.put("Longitude", longitude);
            parentObj.put("UserComments", strComment);
            parentObj.put("RightImages", rightImages);
            parentObj.put("FrontImages", frontImages);
            parentObj.put("LeftImages", leftImages);
            parentObj.put("BackImages", backImages);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Json: "+parentObj.toString());
        return parentObj.toString();
    }

    private class SaveUserRequestApi extends AsyncTask<String, Integer, String> {


        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
       showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleDetailsActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<SaveUserRequestResponse> call = apiService.saveUserRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SaveUserRequestResponse>() {
                @Override
                public void onResponse(Call<SaveUserRequestResponse> call, Response<SaveUserRequestResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        SaveUserRequestResponse saveUserRequestResponse = response.body();
                        try {
                            if(saveUserRequestResponse.getStatus()){
                                //status true case
                                showCompletedDialog(saveUserRequestResponse.getData().getRequestCode());
                            }
                            else {
                                //status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = saveUserRequestResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VehicleDetailsActivity.this);
                                }
                                else {
                                    String failureResponse = saveUserRequestResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), VehicleDetailsActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SaveUserRequestResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(VehicleDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showCompletedDialog(String code){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")){
             layout = R.layout.dialog_submit_;
        }
        else {
            layout = R.layout.dialog_submit_ar;
        }
//        int layout = R.layout.dialog_submit_;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancle);
        TextView dialogbox1 =(TextView) dialogView.findViewById(R.id.thankyou1);
        if (language.equalsIgnoreCase("Ar")){
            dialogbox1.setTypeface(Constants.getarTypeFace(VehicleDetailsActivity.this));
            dialogbox1.setText("شكراً لاستخدامكم سمكره! ارسالنا طلبكم رقم #"+code+"-م ق بنجاح. بنرسل لك عروض من مقدم الخدمة الرهيبين ");
        }
        else {
            dialogbox1.setText("Thank you! Your request #"+code+" has been submitted successfully. You will start receiving offers soon from awesome service providers.");
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VehicleDetailsActivity.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    public String getAddress(double lat, double lng) throws IOException {
        String address = null;
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(lat, lng, 3); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        try {
            address = addresses.get(0).getAddressLine(0);
            strAddress = address;
        } catch (Exception e) {
            e.printStackTrace();
            strAddress = "";
        }
        return address;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VehicleDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}