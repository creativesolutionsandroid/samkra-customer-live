package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {


    Button menu_btn;
    Toolbar toolbar;
    Context context;
    Button buttonSignIn;
    EditText inputMobile;
    EditText inputPassword;
    ImageView back_btn;
    private static final String TAG = "TAG";
    TextView textRegister, FPassword, signintv;
    //    ImageButton imagePasswordEye;
    SharedPreferences userPrefs;
    String strMobile, strPassword;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences LanguagePrefs;
    TextView st1;
    AlertDialog loaderDialog;
    //    EditText inputMobile;
//    SharedPreferences.Editor userPrefsEditor;
//    LinearLayout registerLayout, forgotPasswordLayout;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor = languagePrefs.edit();
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_signin);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_signin_ar);
        }

        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSignIn = (Button) findViewById(R.id.bt1);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        st1 = (TextView) findViewById(R.id.tv1);


//        textRegister = (TextView) findViewById(R.id.signin_register);
        inputMobile = (EditText) findViewById(R.id.phone);
        inputPassword = (EditText) findViewById(R.id.password);
        FPassword = (TextView) findViewById(R.id.Fpassword);
        signintv = (TextView) findViewById(R.id.signintv);

//        imagePasswordEye = (ImageButton) findViewById(R.id.signin_image_password);
//        registerLayout = (LinearLayout) findViewById(R.id.signin_register_layout);
//        forgotPasswordLayout = (LinearLayout) findViewById(R.id.signin_forgot_password);


        FPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        signintv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(i);

            }
        });

//        dlanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (language.equalsIgnoreCase("En")) {
//                    languagePrefsEditor.putString("language", "Ar");
//                    languagePrefsEditor.commit();
//                    Intent i = new Intent(SignInActivity.this, SignInActivity.class);
//                    i.putExtra("type", "1");
//                    startActivity(i);
//                    finish();
//                }else {
//                    languagePrefsEditor.putString("language", "En");
//                    languagePrefsEditor.commit();
//                    Intent i = new Intent(SignInActivity.this, SignInActivity.class);
//                    i.putExtra("type", "1");
//                    startActivity(i);
//                    finish();
//                }
//            }
//        });


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        userLoginApi();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
    }

    private void setTypeface() {
        inputMobile.setTypeface(Constants.getarTypeFace(context));
        inputPassword.setTypeface(Constants.getarTypeFace(context));
        buttonSignIn.setTypeface(Constants.getarTypeFace(context));
        FPassword.setTypeface(Constants.getarTypeFace(context));
        FPassword.setTypeface(Constants.getarTypeFace(context));
        signintv.setTypeface(Constants.getarTypeFace(context));
        st1.setTypeface(Constants.getarTypeFace(context));

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void launchSignUpActivity(View view) {
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivityForResult(intent, SIGNUP_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations() {
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
        strMobile = strMobile.replace("+966 ", "");

        if (strMobile.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            } else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        } else if (strMobile.length() != 9) {
            if (language.equalsIgnoreCase("En")) {
                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            } else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        } else if (strPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));

            }
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }


    private void userLoginApi() {
        String inputStr = prepareLoginJson();

        showloaderAlertDialog();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<Signupresponse> call = apiService.userLogin(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<Signupresponse>() {
            @Override
            public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                if (response.isSuccessful()) {
                    Signupresponse registrationResponse = response.body();
                    Log.d(TAG, "onResponse: " + registrationResponse.toString());
                    try {
                        if (registrationResponse.getStatus()) {
                            //status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getUsername());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilephoto());
                            userPrefsEditor.commit();
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            //status false case
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignInActivity.this);
                            } else {
                                String failureResponse = registrationResponse.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), SignInActivity.this);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody());
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }
                }

                loaderDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Signupresponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }
                }

                loaderDialog.dismiss();
            }
        });
    }

    private String prepareLoginJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Phone", "966" + strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("Language", language);
            parentObj.put("DeviceVersion", "Android");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj.toString());
        return parentObj.toString();
    }

    public void showloaderAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignInActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
