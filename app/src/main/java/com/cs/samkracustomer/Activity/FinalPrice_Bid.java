package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.PlacebidResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.GPSTracker;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.daasuu.bl.BubbleLayout;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalPrice_Bid extends AppCompatActivity {

    BubbleLayout finalPriceLayout;
    ImageView backBtn;
    TextView workshopname,requestcode,distance,carname,carclass,year,dateleft;
    EditText customercomment,wrokshopcomment,customerFinalComment,minimumquote,maximumquote,wrokshopnote,finalprice;
    String TAG = "TAG";
    String strAddress;
    Double latitude, longitude;
    LinearLayout home;
    BubbleLayout workshoplayout;
    ProgressBar progressBar;
    TextView tvResortName, tvResortAddress, tvTodaydate, tvTodayMonth, tvTodayWeek, tvResortPrice, tvCountdownTitle,tvTimeLeft,st1,st2,st3,st4;
    long remainingMillis = 60 * 2880 * 1000;
    CountDownTimer countDownTimer2;
    AlertDialog loaderDialog;
//    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    final Context context = this;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    ArrayList<GetUserNewRequestResponse.Data> bidData = new ArrayList<>();
    int bidPos;
    LinearLayout submitLayout,finalpricelayout,location;
    ImageView redImage;
    RelativeLayout timerLayout;
    AlertDialog customDialog;
    GPSTracker gps;
    private static final int LOCATION_REQUEST = 3;
    private FusedLocationProviderClient mFusedLocationClient;
    ArrayList<GetUserNewRequestResponse.Data> carData = new ArrayList<>();
    int carPos;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] WEEKS = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_final_bid);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_final_bid_ar);
        }

        tvTimeLeft = (TextView) findViewById(R.id.timeLeft);
        requestcode =(TextView)findViewById(R.id.fp_request_code);
        workshopname=(TextView) findViewById(R.id.fp_customer_name);
        distance =(TextView)findViewById(R.id.fp_distance);
        carname =(TextView)findViewById(R.id.fp_car_model);
        carclass =(TextView)findViewById(R.id.fp_s_class);
        year =(TextView)findViewById(R.id.fp_year);
        customercomment=(EditText)findViewById(R.id.fp_user_comment);
        wrokshopcomment=(EditText)findViewById(R.id.fp_wrokshop_comment);
        customerFinalComment = (EditText) findViewById(R.id.fp_user_final_comment);
        minimumquote=(EditText)findViewById(R.id.fp_min_quote);
        maximumquote=(EditText)findViewById(R.id.fp_max_quote);
        wrokshopnote=(EditText)findViewById(R.id.fp_workshop_note);
        finalprice=(EditText)findViewById(R.id.fp_final_quote);
        backBtn=(ImageView)findViewById(R.id.fp_back_btn);
        location=(LinearLayout) findViewById(R.id.fb_location);
        home=(LinearLayout) findViewById(R.id.fp_home_btn);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        timerLayout = (RelativeLayout) findViewById(R.id.timer_layout);
        dateleft=(TextView)findViewById(R.id.fb_date);
        st1 =(TextView)findViewById(R.id.fb_estimated_text);
        st2 =(TextView)findViewById(R.id.fb_customer_text);
        st4=(TextView)findViewById(R.id.request_text);
        finalPriceLayout=(BubbleLayout) findViewById(R.id.workshop_final_comment);
//
//
//        tvTimeLeft = (TextView) findViewById(R.id.timeLeft);
//        tvCountdownTitle = (TextView) findViewById(R.id.day_countdown_text);


        redImage = (ImageView) findViewById(R.id.red_image);
        submitLayout = (LinearLayout) findViewById(R.id.submit_layout);
        finalpricelayout =(LinearLayout) findViewById(R.id.finalpricelayout);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        distance. setPaintFlags(distance.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        bidData = (ArrayList<GetUserNewRequestResponse.Data>) getIntent().getSerializableExtra("data");
        bidPos = getIntent().getIntExtra("bidPos", 0);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        requestcode.setText("#"+bidData.get(bidPos).getRequestcode());
        if (language.equalsIgnoreCase("En")){
            workshopname.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopName_en());
        }
        else {
            workshopname.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopName_ar());
        }

        if (language.equalsIgnoreCase("En")){
            carname.setText(bidData.get(bidPos).getCm().get(0).getCarmakernameen());
            carclass.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getModelnameen());
        }
       else  {
            carname.setText(bidData.get(bidPos).getCm().get(0).getCarmakernamear());
            carclass.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getModelnamear());
        }

         year.setText(""+Constants.convertToArabic(String.valueOf(bidData.get(bidPos).getYear())));
        customercomment.setText(bidData.get(bidPos).getUsercomments());
        wrokshopcomment.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopcomments());
        distance.setText(""+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopdistance()+"KM");
        minimumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote())));
        maximumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote())));
        wrokshopnote.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalPriceWSComment());
        customerFinalComment.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getUserAcceptedComment());
        finalprice.setText(Constants.convertToArabic(Constants.priceFormat1.format(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice())));

        Log.d(TAG, "customerFinalComment"+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getUserAcceptedComment());
        if(bidData.get(bidPos).getRequeststatus().equalsIgnoreCase("Pending Accept Final Price")){
            submitLayout.setVisibility(View.VISIBLE);
        }

        if(bidData.get(bidPos).getRequeststatus().equalsIgnoreCase("Accepted")){
            finalpricelayout.setVisibility(View.GONE);
            timerLayout.setVisibility(View.GONE);
            finalPriceLayout.setVisibility(View.GONE);
        }

        if(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getEstimatedDate() != null){
            calculateMinutesLeft();
            timerLayout.setVisibility(View.VISIBLE);
        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinalPrice_Bid.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        submitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice());
            }

        });
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLatitude()+","+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLongitude()));
                startActivity(intent);

            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(FinalPrice_Bid.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(FinalPrice_Bid.this);
                try {
                    getGPSCoordinates();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(FinalPrice_Bid.this);
            try {
                getGPSCoordinates();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

    }
    private void setTypeface(){
        workshopname.setTypeface(Constants.getarbooldTypeFace(FinalPrice_Bid.this));
        requestcode.setTypeface(Constants.getarbooldTypeFace(FinalPrice_Bid.this));
        distance.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        carname.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        carclass.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        year.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        dateleft.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        tvTimeLeft.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        requestcode.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        workshopname.setTypeface(Constants.getarbooldTypeFace(FinalPrice_Bid.this));
        st1.setTypeface(Constants.getarbooldTypeFace(FinalPrice_Bid.this));
        st2.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        st4.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.yearst)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.carmodelst)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.distancest)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.minimamst)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.maxmiumst)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.finalpricest)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.okst)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        ((TextView)findViewById(R.id.homest)).setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(FinalPrice_Bid.this);
                    try {
                        getGPSCoordinates();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(FinalPrice_Bid.this, "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();

                    }
                   else  {
                        Toast.makeText(FinalPrice_Bid.this, "Location permission denied, Unable to show nearby resorts", Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(FinalPrice_Bid.this, perm));
    }

    public void getGPSCoordinates() {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {
                    mFusedLocationClient.getLastLocation().addOnFailureListener(FinalPrice_Bid.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(FinalPrice_Bid.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    public void showtwoButtonsAlertDialog(float finalPrice){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FinalPrice_Bid.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


        if (language.equalsIgnoreCase("En")){
            title.setText("Samkra");
            yes.setText(getResources().getString(R.string.yes));
            no.setText(getResources().getString(R.string.no));
            desc.setText("Do you want to confirm final price "+Constants.convertToArabic(Constants.priceFormat1.format(finalPrice))+" SR?");

        }
        else {
            title.setText("سمكره");
            yes.setText(getResources().getString(R.string.yes_ar));
            no.setText(getResources().getString(R.string.no_ar));
            desc.setText("هل السعر النهاءي مناسب لك "+Constants.convertToArabic(Constants.priceFormat1.format(finalPrice))+" ريال؟");

        }

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
            desc.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
            yes.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
            no.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(FinalPrice_Bid.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new userAccpet().execute();
                } else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });


        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


    private class  userAccpet extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            showloaderAlertDialog();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(FinalPrice_Bid.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<PlacebidResponce> call = apiService.AcceptUserFinalBidPrice(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PlacebidResponce>() {
                @Override
                public void onResponse(Call<PlacebidResponce> call, Response<PlacebidResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        PlacebidResponce PlacebidResponce = response.body();
                        try {
                            if (PlacebidResponce.getStatus()) {
                                showCompletedDialog ();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = PlacebidResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), FinalPrice_Bid.this);
                                }
                                else {
                                    String failureResponse = PlacebidResponce.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), FinalPrice_Bid.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PlacebidResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(FinalPrice_Bid.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showCompletedDialog () {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En"))
        {
             layout = R.layout.dialog_thankyou_final;
        }
        else {
            layout = R.layout.dialog_thankyou_final_ar;
//            TextView dialogbox;
//            dialogbox=(TextView)findViewById(R.layout.activity_final_bid_ar);
//            if (language.equalsIgnoreCase("Ar")){
//                dialogbox.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
//            }
        }

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.ty_cancle);
        TextView dialogbox1 =(TextView) dialogView.findViewById(R.id.thankyou1);
        if (language.equalsIgnoreCase("Ar")){
//            dialogbox.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
            dialogbox1.setTypeface(Constants.getarTypeFace(FinalPrice_Bid.this));
            dialogbox1.setText("مشكور اخترت السعر اللي يناسبك، الحين بنرسلك فاتوره مرتبه");
        }
        else {
            dialogbox1.setText("Thank you for accepting the final price. You should receive your invoice shortly.");
        }
//        Button directions =(Button) dialogView.findViewById(R.id.dt_btn);
//
//        directions.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLatitude()+","+bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLongitude()));
//                startActivity(intent);
//
//            }
//        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinalPrice_Bid.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.80;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BidId",bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getBidid());
            parentObj.put("UserId",userId);
            parentObj.put("RequestId",bidData.get(bidPos).getRequestid());
            parentObj.put("WorkshopId",bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getWorkshopid());
            Log.d(TAG, "prepareLoginJson: "+parentObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    public void calculateMinutesLeft() {
        Date accept = null, expect = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        String accpetedDate = bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getAcceptedOn();
        String expectedDate = bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getEstimatedDate();
//        dateleft.setText(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getEstimatedDate());


        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sd2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.US);

        try {
            java.util.Date datetime = sdf1.parse(bidData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getEstimatedDate());
            String str123 = sd2.format(datetime);
            dateleft.setText(Constants.convertToArabic(str123));
            Log.d(TAG, "str123 "+sd2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            accept = DateFormat.parse(accpetedDate);
            expect = DateFormat.parse(expectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long startMillis = accept.getTime();
        long currentMillis = calendar.getTimeInMillis();
        long endMillis = expect.getTime();

        if (currentMillis < endMillis) {
            remainingMillis = endMillis - currentMillis;

            long maxMillis = endMillis - startMillis;
            final int maxTime = (int) maxMillis / 60000;
            progressBar.setMax(maxTime);


//        countDownTimer2 = new CountDownTimer(remainingMillis, 60000) {
//            // 1000 means, onTick function will be called at every 1000 milliseconds
//
//            @Override
//            public void onTick(long leftTimeInMilliseconds) {

            final int remTime = (int) remainingMillis / 60000;
            int percent = ((maxTime - remTime) / maxTime) * 100;
            progressBar.setProgress(percent);

            if (remTime > 1440) {
                int days = remTime / 1440;
                if (days == 1) {
                    if (language.equalsIgnoreCase("En")){
                        tvTimeLeft.setText(days + "\nday");
                    }
                    else {
                        tvTimeLeft.setText(days + "\nأيام");
                    }

                } else {
                    if (language.equalsIgnoreCase("En")){
                        tvTimeLeft.setText(days + "\ndays");
                    }
                    else {
                        tvTimeLeft.setText(days + "\n" +
                                "أيام");
                    }

                }
            } else if (remTime > 60) {
                int hours = remTime / 60;
                if (language.equalsIgnoreCase("En")){
                    tvTimeLeft.setText(hours + "\nhours");
                }else {
                    tvTimeLeft.setText(hours + "\nساعات");
                }

            } else {
                tvTimeLeft.setText(remTime + "\nmins");
            }
            dateleft.setTextColor(getResources().getColor(R.color.colorPrimary));
//            }
//            @Override
//            public void onFinish() {
//                tvTimeLeft.setText("");
//            }
//        }.start();
        }
        else {
            dateleft.setTextColor(Color.parseColor("#FF0000"));
            tvTimeLeft.setText("");
            progressBar.setVisibility(View.GONE);
            redImage.setVisibility(View.VISIBLE);
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(FinalPrice_Bid.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


}