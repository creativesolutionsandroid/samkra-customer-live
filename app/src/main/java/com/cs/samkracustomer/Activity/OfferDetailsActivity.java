package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.PlacebidResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.GPSTracker;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferDetailsActivity extends Activity {

    private static final int REQUEST_PHONE_CALL = 1;
    ImageView backBtn,caricon,location;
    TextView carmake,carmodel,wrokshopid,rating,minimumquote,maxmumumquote,year,viewprofile, distance,textCharacters,customercomment,wrokshopcomment,photocount ;
    String TAG = "TAG";
    String BidId,UserId,RequestId,WorkshopId,UserAcceptedComment;
    RatingBar ratingBar;
    String strComment = "No Comments";
    Double latitude, longitude;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    GPSTracker gps;
    AlertDialog loaderDialog;
    private static final int LOCATION_REQUEST = 4;
    private FusedLocationProviderClient mFusedLocationClient;

    LinearLayout bidlist,phone,confirm;
    final Context context = this;
    EditText inputcomment;
    LinearLayout locationLayout;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    AlertDialog customDialog;

    //    ArrayList<OffersResponse.Data> bidData = new ArrayList<>();
    ArrayList<GetUserNewRequestResponse.Data> carData = new ArrayList<>();
    int carPos, bidPos;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.bid_details);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.bid_details_ar);
        }
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

//        bidData = (ArrayList<OffersResponse.Data>) getIntent().getSerializableExtra("bidData");
        carData = (ArrayList<GetUserNewRequestResponse.Data>) getIntent().getSerializableExtra("carData");
        bidPos = getIntent().getIntExtra("bidPos", 0);
        carPos = getIntent().getIntExtra("carPos", 0);
        backBtn =(ImageView) findViewById(R.id.bd_back);
        location =(ImageView) findViewById(R.id.im_location);
        caricon =(ImageView) findViewById(R.id.im_caricon);
        carmake =(TextView) findViewById(R.id.tv_make);
        viewprofile=(TextView) findViewById(R.id.view_profile);
        wrokshopid =(TextView) findViewById(R.id.tv_shopid);
        rating =(TextView) findViewById(R.id.tv_rating);
        maxmumumquote =(TextView) findViewById(R.id.tv_mxq);
        minimumquote =(TextView) findViewById(R.id.tv_mq);
        carmodel =(TextView) findViewById(R.id.tv_modeltex);
        year =(TextView) findViewById(R.id.yearview);
        ratingBar=(RatingBar) findViewById(R.id.ratingbar);
        bidlist=(LinearLayout) findViewById(R.id.bidlist);
        phone=(LinearLayout) findViewById(R.id.phone_icon);
        confirm=(LinearLayout) findViewById(R.id.pf_confirm_icon);
        inputcomment=(EditText) findViewById(R.id.ed_commadview);
        distance = (TextView) findViewById(R.id.distance);
        locationLayout=(LinearLayout) findViewById(R.id.locationl);
        textCharacters=(TextView)findViewById(R.id.comment_chars);
        customercomment=(EditText)findViewById(R.id.fp_user_comment);
        wrokshopcomment=(EditText)findViewById(R.id.fp_wrokshop_comment);
        photocount=(TextView) findViewById(R.id.photo_count);

        inputcomment.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputcomment.setRawInputType(InputType.TYPE_CLASS_TEXT);
        inputcomment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (language.equalsIgnoreCase("En")){
                    textCharacters.setText((300 - (text.length()))+" Characters left");
                }
                else {
                    textCharacters.setText((300 - (text.length()))+"حرف متبقي");
                }

                if (text.toString().length() > 0) {
                    strComment = text.toString();
                }
                else {
                    strComment = "No Comments";
                }
            }
        });

        viewprofile.setPaintFlags(viewprofile.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        distance.setPaintFlags(distance.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        minimumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(carData.get(carPos).getBidlist().get(bidPos).getMinquote())));
        maxmumumquote.setText(Constants.convertToArabic(Constants.priceFormat1.format(carData.get(carPos).getBidlist().get(bidPos).getMaxquote())));
        rating.setText(carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getAveragerating()+"/5");

        if (language.equalsIgnoreCase("En")){
            carmake.setText(carData.get(carPos).getCm().get(0).getCarmakernameen());
            carmodel.setText(carData.get(carPos).getCm().get(0).getMdl().get(0).getModelnameen());
            wrokshopid.setText(""+carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getWorkshopnameEn());
            year.setText(carData.get(carPos).getYear()+" Model");
        }
        else {
            carmake.setText(carData.get(carPos).getCm().get(0).getCarmakernamear());
            carmodel.setText(carData.get(carPos).getCm().get(0).getMdl().get(0).getModelnamear());
            wrokshopid.setText(""+carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getWorkshopnameAr());
            year.setText(carData.get(carPos).getYear()+" نوع السيارة");
        }

//        Log.d(TAG, "onCreate: "+bidPos);
        customercomment.setText(carData.get(0).getUsercomments());
        wrokshopcomment.setText(carData.get(carPos).getCm().get(0).getMdl().get(0).getRb().get(bidPos).getWorkshopcomments());
        photocount.setText(""+carData.get(carPos).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size());

        ratingBar.setRating(carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getAveragerating());
        distance.setText(Constants.priceFormat.format(carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getWorkshopdistance())+" KM");

        Glide.with(OfferDetailsActivity.this)
                .load(Constants.IMAGE_URL+carData.get(carPos).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumentlocation())
                .into(caricon);
        Intent intent = new Intent(OfferDetailsActivity.this, FinalPrice_Bid.class);
        bidlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        backBtn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (validations()) {
                showtwoButtonsAlertDialog();
//                }
            }
        });

        viewprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OfferDetailsActivity.this, ProfileActivity.class);
                i.putExtra("id", carData.get(carPos).getBidlist().get(bidPos).getWorkshopid());
                startActivity(i);
            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        String mobile = carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getMobileno();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                        if (ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    String mobile = carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getMobileno();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                    startActivity(intent);
                }
            }
        });

        locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+carData.get(bidPos).getLatitude()+","+carData.get(bidPos).getLongitude()));
                startActivity(intent);

            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(OfferDetailsActivity.this);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                gps = new GPSTracker(OfferDetailsActivity.this);
                try {
                    getGPSCoordinates();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps = new GPSTracker(OfferDetailsActivity.this);
            try {
                getGPSCoordinates();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }



    private void setTypeface(){
        carmake.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        carmodel.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        wrokshopid.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        rating.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        minimumquote.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        year.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        maxmumumquote.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        viewprofile.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        distance.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        customercomment.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        wrokshopcomment.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        textCharacters.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        textCharacters.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        textCharacters.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        textCharacters.setTypeface(Constants.getarbooldTypeFace(OfferDetailsActivity.this));
        rating.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.textlayout)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.locationtext)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.mm)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.mq)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.ed_commadview)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        ((TextView)findViewById(R.id.comment_chars)).setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
    }



    private boolean canAccessLocation() {
        return (hasPermission1(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, perm));
    }

    public void getGPSCoordinates() {

        if(gps != null){
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {
                    mFusedLocationClient.getLastLocation().addOnFailureListener(OfferDetailsActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("TAG","fused lat null");
                        }
                    })
                            .addOnSuccessListener(OfferDetailsActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        Log.i("TAG","fused lat "+location.getLatitude());
                                        Log.i("TAG","fused long "+location.getLongitude());
                                    }
                                    else{
                                        Log.i("TAG","fused lat null");
                                    }
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    public void showtwoButtonsAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OfferDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        if (language.equalsIgnoreCase("En")){
            title.setText("Samkra");
            yes.setText(getResources().getString(R.string.yes));
            no.setText(getResources().getString(R.string.no));
            desc.setText("Do you want to accept the bid?");

        }
        else {
            title.setText("سمكره");
            yes.setText(getResources().getString(R.string.yes_ar));
            no.setText(getResources().getString(R.string.no_ar));
            desc.setText("هل العرض مناسب لك؟");

        }
        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
            desc.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
            yes.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
            no.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new userAccpet().execute();
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(OfferDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(OfferDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, perm));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    String mobile =carData.get(carPos).getBidlist().get(bidPos).getWks().get(0).getMobileno();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                    if (ActivityCompat.checkSelfPermission(OfferDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(OfferDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(OfferDetailsActivity.this);
                    try {
                        getGPSCoordinates();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
    }


    //    private boolean validations() {
//        strComment = inputcomment.getText().toString();
//        if (strComment.length() == 0) {
//            if (language.equalsIgnoreCase("En")){
//                inputcomment.setError(getResources().getString(R.string.enter_command));
//            }
//            else {
//                inputcomment.setError(getResources().getString(R.string.enter_command_ar));
//            }
//
//            return false;
//        }
//        return true;
//    }

    private class userAccpet extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OfferDetailsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<PlacebidResponce> call = apiService.userAccpet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PlacebidResponce>() {
                @Override
                public void onResponse(Call<PlacebidResponce> call, Response<PlacebidResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        PlacebidResponce PlacebidResponce = response.body();
                        try {
                            if (PlacebidResponce.getStatus()) {
                                showCompletedDialog ();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = PlacebidResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), OfferDetailsActivity.this);
                                }
                                else {
                                    String failureResponse = PlacebidResponce.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), OfferDetailsActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PlacebidResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(OfferDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private void showCompletedDialog () {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")){
            layout = R.layout.dialog_bid_thankyou;
        }
        else {
            layout = R.layout.dialog_bid_thankyou_ar;
//            TextView dialogbox;
//            dialogbox=(TextView)findViewById(R.layout.dialog_bid_thankyou_ar);
//            if (language.equalsIgnoreCase("Ar")){
//                dialogbox.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
//            }

        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        ImageView cancel = (ImageView) dialogView.findViewById(R.id.ty_cancle);
        TextView requstcode =(TextView)dialogView.findViewById(R.id.requstcode);
//        TextView dialogbox =(TextView) dialogView.findViewById(R.id.thankyou);
        TextView dialogbox1 =(TextView) dialogView.findViewById(R.id.requstcode);
        if (language.equalsIgnoreCase("Ar")){
//            dialogbox.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
            dialogbox1.setTypeface(Constants.getarTypeFace(OfferDetailsActivity.this));
        }
        Button directions =(Button) dialogView.findViewById(R.id.dt_btn);

        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "onClick: ");
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+carData.get(bidPos).getLatitude()+","+carData.get(bidPos).getLongitude()));
//                Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+carData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLatitude()+","+carData.get(bidPos).getCm().get(0).getMdl().get(0).getRb().get(0).getLongitude()));
                startActivity(intent);
            }
        });
        if (language.equalsIgnoreCase("En")){
            requstcode.setText("Thank you for accepting the bid for request #"+carData.get(carPos).getBidlist().get(bidPos).getRequestcode()+" Please make your way to the Service Provider");
        }
        else {
            requstcode.setText("شكرا لك على قبول عرض الطلب.#"+carData.get(carPos).getBidlist().get(bidPos).getRequestcode()+"يرجى الذهاب إلى مقدم الخدمة");
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OfferDetailsActivity.this, MainActivity.class);
                intent.putExtra("type", "1");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.75;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("BidId",carData.get(carPos).getBidlist().get(bidPos).getRbid());
            parentObj.put("UserId",userId);
            parentObj.put("RequestId",carData.get(carPos).getBidlist().get(bidPos).getRequestid());
            parentObj.put("WorkshopId",carData.get(carPos).getBidlist().get(bidPos).getWorkshopid());
            parentObj.put("UserAcceptedComment",strComment);
            Log.d(TAG, "prepareLoginJson: "+parentObj.toString());
            Log.d(TAG, "UserAcceptedComment: "+strComment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OfferDetailsActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}

