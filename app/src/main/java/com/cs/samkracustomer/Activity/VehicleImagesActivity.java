package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.ImagesUploadedAdapter;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

;
public class VehicleImagesActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    public static ImageView imageView;
    public static Bitmap selectedBitmap;
    private LinearLayout deleteLayout;

    LinearLayout frontLayout, backLayout, rightLayout, leftLayout;
    ImageView frontSide, backSide, rightSide, leftSide, back_btn;
    LinearLayout galleryLayout;
    ImageView captureIcon, delet_btn, nexticon;
    ViewPager viewPager;
    ImagesUploadedAdapter mAdapter;
    AlertDialog loaderDialog;

    Boolean isImageUploaded = false;
    int sideSelected = 2;
    ArrayList<Bitmap> leftImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> rightImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> frontImagesSelected = new ArrayList<>();
    ArrayList<Bitmap> backImagesSelected = new ArrayList<>();
    Bitmap thumbnail;
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    public static Bitmap firstImage = null;
    String leftImages = "", rightImages = "", frontImages = "", backImages = "";
    boolean isCamera = false;

    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    private int i = 1;
    Boolean pictureTaken = false;
    Uri imageUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_vehicle_images);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.activity_vehicle_images_ar);
        }
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
//
        frontLayout = (LinearLayout) findViewById(R.id.front_layout);
        backLayout = (LinearLayout) findViewById(R.id.back_layout);
        rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        delet_btn = (ImageView) findViewById(R.id.delet_btn);
        nexticon = (ImageView) findViewById(R.id.nexticon);
//        addPhoto = (ImageView) findViewById(R.id.add_photo);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        frontSide = (ImageView) findViewById(R.id.front_side);
        backSide = (ImageView) findViewById(R.id.back_side);
        leftSide = (ImageView) findViewById(R.id.left_side);
        rightSide = (ImageView) findViewById(R.id.right_side);

        imageView = (ImageView) this.findViewById(R.id.piceview);
        captureIcon = (ImageView) this.findViewById(R.id.capture_icon);

        galleryLayout = (LinearLayout) findViewById(R.id.gallery_icon);
        captureIcon = (ImageView) findViewById(R.id.capture_icon);

//        deleteLayout.setVisibility(View.INVISIBLE);
        captureIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        nexticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isImageUploaded) {
                    if (!userPrefs.getString("userId","").equals("")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleImagesActivity.this);
                        if (language.equalsIgnoreCase("En")) {
                            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                new uploadImagesApi().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                new uploadImagesApi().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        Intent intent = new Intent(VehicleImagesActivity.this, SignInActivity.class);
                        startActivityForResult(intent, 6);
                    }
                }
                else{
                    if (language.equalsIgnoreCase("En")){

                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images),
                                getResources().getString(R.string.error),
                                getResources().getString(R.string.ok), VehicleImagesActivity.this);
                    }
                    else {

                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.signup_msg_upload_images_ar),
                                getResources().getString(R.string.error_ar),
                                getResources().getString(R.string.ok_ar), VehicleImagesActivity.this);
                    }
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        frontLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 3;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front_selected));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        backSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 4;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back_selected));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        leftSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 1;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left_selected));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right));
            }
        });

        rightSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sideSelected = 2;
                setImages();
                frontSide.setImageDrawable(getResources().getDrawable(R.drawable.front));
                backSide.setImageDrawable(getResources().getDrawable(R.drawable.back));
                leftSide.setImageDrawable(getResources().getDrawable(R.drawable.left));
                rightSide.setImageDrawable(getResources().getDrawable(R.drawable.right_selected));
            }
        });

//        deleteLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(sideSelected == 1){
//                    leftImagesSelected.remove(selectedBitmap);
//                }
//                else if(sideSelected == 2){
//                    rightImagesSelected.remove(selectedBitmap);
//                }
//                else if(sideSelected == 3){
//                    frontImagesSelected.remove(selectedBitmap);
//                }
//                else if(sideSelected == 4){
//                    backImagesSelected.remove(selectedBitmap);
//                }
//                setImages();
//            }
//        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        ((TextView)findViewById(R.id.car_back)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
        ((TextView)findViewById(R.id.car_left)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
        ((TextView)findViewById(R.id.car_front)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
        ((TextView)findViewById(R.id.car_right)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
        ((TextView)findViewById(R.id.tf_next)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
        ((TextView)findViewById(R.id.tf_galary)).setTypeface(Constants.getarTypeFace(VehicleImagesActivity.this));
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(VehicleImagesActivity.this, perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera=false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    if(isCamera){
                        openCamera();
                    }
                    else {
                        openGallery();
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(VehicleImagesActivity.this, "Storage permission denied, Unable to select pictures", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(VehicleImagesActivity.this, "تم رفض  التخزين ، غير قادر على تحديد الصور", Toast.LENGTH_LONG).show();

                    }
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    openCamera();
                }
                break;
        }
    }

    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessCamera()) {
                isCamera=true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, CAMERA_REQUEST);
//                Intent intent = new Intent(VehicleImagesActivity.this, CameraActivity.class);
//                startActivity(intent);
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAMERA_REQUEST);
//            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            convertCameraImage();
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertGalleryImage();
        }

        if(requestCode == 6 && resultCode == RESULT_OK){
            String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleImagesActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new uploadImagesApi().execute();
            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertCameraImage() {
        String path = getRealPathFromURI(imageUri);

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inSampleSize = 8;
        bmOptions.inScaled = false;

        thumbnail = BitmapFactory.decodeFile(path, bmOptions);

        Matrix matrix = new Matrix();

        matrix.postRotate(90);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, 900,
                800, true);

        thumbnail = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        Log.d("TAG", "getByteCount: "+thumbnail.getByteCount());

        imageView.setImageBitmap(thumbnail);
        selectedBitmap = thumbnail;
        if(sideSelected == 1) {
            leftImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 2) {
            rightImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 3) {
            frontImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 4) {
            backImagesSelected.add(thumbnail);
        }
        setImages();
    }

    private void convertGalleryImage() {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 900, 800,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.PNG, 100);
        Log.d("TAG", "getByteCount: "+thumbnail.getByteCount());

        imageView.setImageBitmap(thumbnail);
        selectedBitmap = thumbnail;
        if(sideSelected == 1) {
            leftImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 2) {
            rightImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 3) {
            frontImagesSelected.add(thumbnail);
        }
        else if(sideSelected == 4) {
            backImagesSelected.add(thumbnail);
        }
        setImages();
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private void setImages() {
        ArrayList<Bitmap> imagesSelected = new ArrayList<>();

        if(sideSelected == 1) {
            imagesSelected.addAll(leftImagesSelected);
        }
        else if(sideSelected == 2) {
            imagesSelected.addAll(rightImagesSelected);
        }
        else if(sideSelected == 3) {
            imagesSelected.addAll(frontImagesSelected);
        }
        else if(sideSelected == 4) {
            imagesSelected.addAll(backImagesSelected);
        }

        mAdapter = new ImagesUploadedAdapter(VehicleImagesActivity.this, imagesSelected, language);
        viewPager.setAdapter(mAdapter);
        if(imagesSelected.size()>0){
            isImageUploaded = true;
//            deleteLayout.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(imagesSelected.get(0));
        }
        else{
//            deleteLayout.setVisibility(View.INVISIBLE);
            imageView.setImageBitmap(null);
        }
    }

    private class uploadImagesApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... str) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VehicleImagesActivity.this);

            HttpResponse httpResponse = null;
            int count = 0, usedCount = 0;

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);
            HttpPost httppost = new HttpPost(Constants.IMAGE_UPLOAD_URL);


            try{
                for (int i = 0; i < rightImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = rightImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = rightImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "right");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < leftImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = leftImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = leftImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "left");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < frontImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = frontImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = frontImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "front");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                for (int i = 0; i < backImagesSelected.size(); i++) {
                    Bitmap thumbnail1 = null;
                    String imagePath1 = "";
                    File file=null;
                    thumbnail1 = backImagesSelected.get(i);
                    if(firstImage == null){
                        firstImage = backImagesSelected.get(i);
                    }
                    if (thumbnail1 != null) {
                        imagePath1 = StoreByteImage(thumbnail1, "back");
                        count = count + 1;
                    }

                    if (imagePath1 != null) {
                        file = new File(imagePath1);
                    }

                    if (null != file && file.exists()) {
                        multipartEntity.addPart("image", new FileBody(file));
                    }
                }

                httppost.setEntity(multipartEntity);
                httpResponse=  mHttpClient11.execute(httppost);
            }catch(Exception e){
                e.printStackTrace();
            }

            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "doInBackground: " + convertInputStreamToString(entity));
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO Auto-generated method stub
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
                    Log.d("TAG", "onPostExecute: "+imageResponse);
                    JSONArray resultArray = new JSONArray(imageResponse);
                    for (int i = 0; i < resultArray.length(); i++){
                        JSONObject jsonObject = resultArray.getJSONObject(i);
                        if(!jsonObject.getString("Name").equals("")){
                            Intent intent = new Intent(VehicleImagesActivity.this, VehicleDetailsActivity.class);
//                            intent.putExtra("logo", firstImage);
                            intent.putExtra("left", leftImages);
                            intent.putExtra("right", rightImages);
                            intent.putExtra("front", frontImages);
                            intent.putExtra("back", backImages);
                            startActivity(intent);
                            break;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(VehicleImagesActivity.this, "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(VehicleImagesActivity.this, "خطأ في تحميل الصور ، الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
            if(loaderDialog!=null){
                loaderDialog.dismiss();
            }
            super.onPostExecute(s);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        if(type.equals("left") ) {
            pictureName = "LEFT_" + userId + "_" + timeStamp + ".jpg";
            if(leftImages.equals("")){
                leftImages = pictureName;
            }
            else {
                leftImages = leftImages + "," +pictureName;
            }
        }
        else if(type.equals("right") ) {
            pictureName = "RIGHT_" + userId + "_" + timeStamp + ".jpg";
            if(rightImages.equals("")){
                rightImages = pictureName;
            }
            else {
                rightImages = rightImages + "," +pictureName;
            }
        }
        else if(type.equals("front") ) {
            pictureName = "FRONT_" + userId + "_" + timeStamp + ".jpg";
            if(frontImages.equals("")){
                frontImages = pictureName;
            }
            else {
                frontImages = frontImages + "," +pictureName;
            }
        }
        else if(type.equals("back") ) {
            pictureName = "BACK_" + userId + "_" + timeStamp + ".jpg";
            if(backImages.equals("")){
                backImages = pictureName;
            }
            else {
                backImages = backImages + "," +pictureName;
            }
        }

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "samkra");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VehicleImagesActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}

