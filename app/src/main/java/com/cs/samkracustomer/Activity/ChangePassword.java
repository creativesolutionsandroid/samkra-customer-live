package com.cs.samkracustomer.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePassword extends Activity {

    private String response12 = null;
    ProgressDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    Button submit;
    SharedPreferences userPrefs;
    String response, userId,strPhone,strPassword,strNewpassword;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor userPrefEditor;
    String language;
    ImageView backBtn;
    AlertDialog loaderDialog;

    SharedPreferences.Editor languagePrefsEditor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.change_password);
        }
        else {
            setContentView(R.layout.change_password_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        strPhone = userPrefs.getString("mobile", null);

        backBtn = (ImageView) findViewById(R.id.back_btn);

        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        submit = (Button) findViewById(R.id.change_password_button);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                strPassword = oldPwd;
                strNewpassword = newPwd;
                if (oldPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")){
                        oldPassword.setError("Please enter old password");
                    }
                    else {
                        oldPassword.setError("الرجاء إدخال كلمة المرور");
                    }

                } else if (newPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError("Please enter new password");
                    } else {
                        newPassword.setError("الرجاء ادخال كلمة المرور الجديدة");
                    }
                } else if (newPwd.length() < 4 || newPwd.length() > 20) {
                    if (language.equalsIgnoreCase("En")) {
                        if (language.equalsIgnoreCase("En")) {
                            newPassword.setError("Password must be 4 to 20 characters");
                        } else {
                            newPassword.setError("يجب أن تتكون كلمة المرور من 4 إلى 20 حرفًا");
                        }
                    }
                } else if (confirmPwd.length() == 0) {
                    if (language.equalsIgnoreCase("En")){
                        confirmPassword.setError("Please retype password");
                    } else {
                        confirmPassword.setError("الرجاء أعد ادخال كلمة المرور");
                    }
                } else if (!newPwd.equals(confirmPwd)) {
                    if (language.equalsIgnoreCase("En")) {
                        confirmPassword.setError("Passwords not match, please retype");
                    } else {
                        confirmPassword.setError("كلمة المرور غير مطابقة، يرجى إعادة كلمة المرور");
                    }
                } else {
                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }
    private void setTypeface(){
        ((TextView)findViewById(R.id.tv1)).setTypeface(Constants.getarTypeFace(ChangePassword.this));
        oldPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        newPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        confirmPassword.setTypeface(Constants.getarTypeFace(ChangePassword.this));
        submit.setTypeface(Constants.getarTypeFace(ChangePassword.this));

    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Phone",""+strPhone);
            parentObj.put("Password",strPassword);
            parentObj.put("NewPassword", strNewpassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG","prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class ChangePasswordResponse extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.changepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        Signupresponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getUserid();
                                userPrefEditor.putString("userId", userId);
                                userPrefEditor.putString("name", resetPasswordResponse.getData().getUsername());
                                userPrefEditor.putString("email", resetPasswordResponse.getData().getEmail());
                                userPrefEditor.putString("mobile", resetPasswordResponse.getData().getPhone());
                                userPrefEditor.commit();
                                finish();
//                                Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
//                                ForgotPasswordActivity.isResetSuccessful = true;
//                                getDialog().dismiss();
                            }
                            else {
//                                status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ChangePassword.this);
                                }
                                else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), ChangePassword.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(ChangePassword.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(ChangePassword.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ChangePassword.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ChangePassword.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ChangePassword.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ChangePassword.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(loaderDialog != null){
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePassword.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
