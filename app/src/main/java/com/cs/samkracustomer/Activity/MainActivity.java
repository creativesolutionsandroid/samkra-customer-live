package com.cs.samkracustomer.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Fragements.HistoryFragment;
import com.cs.samkracustomer.Fragements.HomeFragment;
import com.cs.samkracustomer.Fragements.InboxFragment;
import com.cs.samkracustomer.Fragements.MoreFragment;
import com.cs.samkracustomer.Fragements.MyProfileFragment;
import com.cs.samkracustomer.Fragements.NotificationFragment;
import com.cs.samkracustomer.Models.LanguageResponce;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.SideMenuAdapter;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    //    private DrawerLayout mDrawerLayout;
    LinearLayout mDrawerLinear;
    String[] sideMenuItems;
    Integer[] sideMenuImages;
    ListView sideMenuListView;
    SideMenuAdapter mSideMenuAdapter;
    int itemSelectedPostion = 0;
    public static DrawerLayout drawer;
    private static final String TAG = "TAG";
    AlertDialog loaderDialog;
    FragmentManager fragmentManager = getSupportFragmentManager();
    Button workshop;

    ImageView closeMenu, logout;
    TextView userName;
    public static CircleImageView profilePic;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    AlertDialog customDialog;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_main_ar);
        }
        super.onCreate(savedInstanceState);

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                if (drawer.isDrawerOpen(GravityCompat.START)) {
//                    drawer.closeDrawer(GravityCompat.START);
//
//                } else {
//                    drawer.openDrawer(GravityCompat.START);
//                }
//            }
//        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(R.id.left_drawer);
        closeMenu = (ImageView) findViewById(R.id.close_btn);
        logout = (ImageView) findViewById(R.id.logout_btn);
        userName = (TextView) findViewById(R.id.username);
        profilePic = (CircleImageView) findViewById(R.id.profile_pic);
        workshop = (Button) findViewById(R.id.workshop);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (getIntent().getStringExtra("type").equals("2")) {
            NotificationFragment homeScreenFragment = new NotificationFragment();
            fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
            fragmentTransaction.commit();
        } else {
            HomeFragment homeScreenFragment = new HomeFragment();
            fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
            fragmentTransaction.commit();
        }

        if (language.equalsIgnoreCase("En")) {
            sideMenuItems = new String[]{getResources().getString(R.string.menu_home),
                    getResources().getString(R.string.menu_invoices),
                    getResources().getString(R.string.menu_inbox),
                    getResources().getString(R.string.menu_notification),
                    getResources().getString(R.string.menu_profile),
                    getResources().getString(R.string.menu_AboutUscreen),
                    getResources().getString(R.string.menu_language)
            };
        } else {
            sideMenuItems = new String[]{getResources().getString(R.string.menu_home_ar),
                    getResources().getString(R.string.menu_invoices_ar),
                    getResources().getString(R.string.menu_inbox_ar),
                    getResources().getString(R.string.menu_notification_ar),
                    getResources().getString(R.string.profile_ar),
                    getResources().getString(R.string.menu_AboutUscreen_ar),
                    getResources().getString(R.string.menu_language_ar)
            };
        }
        sideMenuImages = new Integer[]{R.drawable.home_ar,
                R.drawable.invoice_ar,
                R.drawable.inbox_ar,
                R.drawable.notification_ar,
                R.drawable.profile_ar,
                R.drawable.main,
                R.drawable.language_ar};

        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        if (language.equalsIgnoreCase("En")) {
            mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);

        } else {
            mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu_ar, sideMenuItems, sideMenuImages, itemSelectedPostion);

        }
        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        workshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = "com.cs.samkraworkshop"; // package name of the app
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, MyProfileFragment.class));
                    FragmentManager fragmentManager1 = getSupportFragmentManager();

                    Fragment historyfrahmet = new MyProfileFragment();
                    fragmentManager1.beginTransaction().replace(R.id.fragment_layout, historyfrahmet).commit();
                    if (language.equalsIgnoreCase("En")) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.closeDrawer(GravityCompat.END);

                    }
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 4);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
            }
        });

        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, MyProfileFragment.class));
                    FragmentManager fragmentManager1 = getSupportFragmentManager();

                    Fragment historyfrahmet = new MyProfileFragment();
                    fragmentManager1.beginTransaction().replace(R.id.fragment_layout, historyfrahmet).commit();
                    if (language.equalsIgnoreCase("En")) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.closeDrawer(GravityCompat.END);

                    }
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 4);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
            }
        });

        if (language.equalsIgnoreCase("Ar")) {
            setTypeface();
        }
    }

    private void setTypeface() {
        ((TextView) findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(MainActivity.this));
        ((TextView) findViewById(R.id.workshop)).setTypeface(Constants.getarTypeFace(MainActivity.this));
        userName.setTypeface(Constants.getarTypeFace(MainActivity.this));
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//            case android.R.id.home:
////                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
////                    drawer.closeDrawer(Gravity.RIGHT);
////                } else {
////                    drawer.openDrawer(Gravity.RIGHT);
////                }
//                menuClick();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equalsIgnoreCase("En")) {
            title.setText("Samkra");
            yes.setText(getResources().getString(R.string.yes));
            no.setText(getResources().getString(R.string.no));
            desc.setText("Do you want to logout?");
        } else {
            title.setText(getResources().getString(R.string.samkra_ar));
            yes.setText(getResources().getString(R.string.yes_ar));
            no.setText(getResources().getString(R.string.no_ar));
            desc.setText("هل ترغب بسجيل الخروج؟");

        }

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(MainActivity.this));
            desc.setTypeface(Constants.getarTypeFace(MainActivity.this));
            yes.setTypeface(Constants.getarTypeFace(MainActivity.this));
            no.setTypeface(Constants.getarTypeFace(MainActivity.this));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userPrefsEditor.clear();
                userPrefsEditor.commit();
                customDialog.dismiss();
                if (language.equalsIgnoreCase("En")){
                    userName.setText("Sign In");
                }
                else {
                    userName.setText("الدخول للحساب");
                }

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }

                profilePic.setImageResource(R.drawable.main);
                logout.setVisibility(View.GONE);

                Intent i = new Intent(MainActivity.this, MainActivity.class);
                i.putExtra("type", "1");
                startActivity(i);
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            itemSelectedPostion = position;
            if (position != 4 && position != 6 && position != 7 && position != 8) {
//                mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
                if (language.equalsIgnoreCase("En")) {
                    mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);

                } else {
                    mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu_ar, sideMenuItems, sideMenuImages, itemSelectedPostion);

                }
                sideMenuListView.setAdapter(mSideMenuAdapter);
                mSideMenuAdapter.notifyDataSetChanged();
            }
        }
    }

    public void selectItem(int position) {
        switch (position) {
            case 0:
                Fragment mainFragment = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 1:
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, HistoryFragment.class));
                    Fragment historyfrahmet = new HistoryFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, historyfrahmet).commit();
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 1);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;


            case 2:
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, InboxFragment.class));
                    Fragment inboxfrahmet = new InboxFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxfrahmet).commit();
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 2);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 3:
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, NotificationFragment.class));
                    Fragment notificationfrahmet = new NotificationFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationfrahmet).commit();
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 3);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 4:
                if (!userPrefs.getString("userId", "").equals("")) {
//                    startActivity(new Intent(MainActivity.this, MyProfileFragment.class));
                    Fragment myProfileFragment = new MyProfileFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, myProfileFragment).commit();
                } else {
                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.putExtra("type", "1");
                    startActivityForResult(intent, 4);
                }
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;

            case 5:
//                startActivity(new Intent(MainActivity.this, MoreFragment.class));
                Fragment moreFragment = new MoreFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, moreFragment).commit();
                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 6:
                language = languagePrefs.getString("language", "En");
                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = "Ar";
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = "En";
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    finish();
                }
        }
    }

    public void menuClick() {
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
//            Intent intent = new Intent(MainActivity.this, HistoryFragment.class);
            Fragment historyfrahmet = new HistoryFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, historyfrahmet).commit();
        } else if (requestCode == 2 && resultCode == RESULT_OK) {
//            Intent intent = new Intent(MainActivity.this, InboxFragment.class);
            Fragment inboxfrahmet = new InboxFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxfrahmet).commit();
        } else if (requestCode == 3 && resultCode == RESULT_OK) {
//            Intent intent = new Intent(MainActivity.this, NotificationFragment.class);
            Fragment notificationfrahmet = new NotificationFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationfrahmet).commit();
        } else if (requestCode == 4 && resultCode == RESULT_OK) {
//            Intent intent = new Intent(MainActivity.this, MyProfileFragment.class);
            Fragment myProfileFragment = new MyProfileFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, myProfileFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!userPrefs.getString("userId", "").equals("")) {
            userName.setText(userPrefs.getString("name", ""));
            if(!userPrefs.getString("pic", "").equalsIgnoreCase("")) {
                Glide.with(MainActivity.this)
                        .load(Constants.IMAGE_URL + userPrefs.getString("pic", ""))
                        .into(profilePic);
            }
            else {
                profilePic.setImageResource(R.drawable.main);
            }
            logout.setVisibility(View.VISIBLE);
            userLoginApi();
        } else {
            if (language.equalsIgnoreCase("En")) {
                userName.setText("Sign In");
            } else {
                userName.setText("الدخول للحساب");
            }

            profilePic.setImageResource(R.drawable.main);
            logout.setVisibility(View.GONE);
        }
    }


    private void userLoginApi() {
        String inputStr = prepareLoginJson();

//        showloaderAlertDialog();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<LanguageResponce> call = apiService.langyagechange(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<LanguageResponce>() {
            @Override
            public void onResponse(Call<LanguageResponce> call, Response<LanguageResponce> response) {
                Log.d(TAG, "onResponse: " + response);
                if (response.isSuccessful()) {

                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody());
                }

//                loaderDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LanguageResponce> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

//                loaderDialog.dismiss();
            }
        });
    }
    private String prepareLoginJson () {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("UserId", userPrefs.getString("userId",""));
            language = languagePrefs.getString("language", "En");
            if (language.equalsIgnoreCase("En")) {
                parentObj.put("Language", "En");
            } else {
                parentObj.put("Language", "Ar");
            }
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("Language", language);
            parentObj.put("DeviceVersion", "Android");
            parentObj.put("UserType", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj.toString());
        return parentObj.toString();
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}



