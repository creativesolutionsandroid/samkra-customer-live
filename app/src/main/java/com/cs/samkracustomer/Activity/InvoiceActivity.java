package com.cs.samkracustomer.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Adapters.InvoiceItemAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.InvoiceResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceActivity extends AppCompatActivity {

    ImageView backBtn, share, print, download;
    TextView order_number, date, workshopadd, workshopadd1,custmorename,totalamt, vatamt, subamt, discountamt, netamt, finalamt;
    String TAG = "TAG";
    String Language = "En";
    ArrayList<AccpetedOffersResponce.Data> data = new ArrayList<>();
    String userId;
    File file12;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    AlertDialog loaderDialog;

    int invoicePos;
    ScrollView mscrollview;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    LinearLayout itemsLayout;
    RelativeLayout rateLayout;
    TextView skipRating;
    RatingBar ratingBar;

    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.invoices);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.invoices_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        data = (ArrayList<AccpetedOffersResponce.Data>) getIntent().getSerializableExtra("data");
        invoicePos = getIntent().getIntExtra("pos", 0);

        backBtn = findViewById(R.id.hy_back_btn);
        share = findViewById(R.id.share);
        download = findViewById(R.id.download);

        rateLayout = (RelativeLayout) findViewById(R.id.rate_layout);
        skipRating = (TextView) findViewById(R.id.skip_rating);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);

        totalamt = findViewById(R.id.totalamt);
        vatamt = findViewById(R.id.vatamt);
        subamt = findViewById(R.id.subamt);
        discountamt = findViewById(R.id.discountamt);
        netamt = findViewById(R.id.netamt);
        finalamt = findViewById(R.id.finalamt);
//        comment = findViewById(R.id.comment);
        order_number = findViewById(R.id.order_number);
        date = findViewById(R.id.date);
        workshopadd = findViewById(R.id.worker_shop_add);
        workshopadd1 = findViewById(R.id.worker_shop_add1);
        itemsLayout = (LinearLayout) findViewById(R.id.item_list);
        custmorename=(TextView)findViewById(R.id.in_name) ;

        mscrollview = findViewById(R.id.scrollView);

        data = (ArrayList<AccpetedOffersResponce.Data>) getIntent().getSerializableExtra("data");
        invoicePos = getIntent().getIntExtra("pos", 0);

        String invoice = data.get(invoicePos).getInvoiceno();
//        String[] invoice1 = invoice.split("-");

        order_number.setText("#" + invoice);

        String str = data.get(invoicePos).getCreatedon();
        String[] array = str.split(" ");
        Log.d(TAG, "date"+data.get(invoicePos).getCreatedon());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);

        try {
            java.util.Date datetime = sdf.parse(array[0]);
            String str123 = sdf1.format(datetime);
            date.setText(Constants.convertToArabic(str123));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (language.equalsIgnoreCase("En")){
            workshopadd.setText("" + data.get(invoicePos).getWs().get(0).getWorkshopnameen());
        }
        else {
            workshopadd.setText("" + data.get(invoicePos).getWs().get(0).getWorkshopnamear());
        }

        workshopadd1.setText(data.get(invoicePos).getWs().get(0).getAddress());
        totalamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getTotalamount())));
        vatamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getVatcharges())));
        subamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getSubtotal())));
        discountamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getDiscount())));
        netamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getNetamount())));
        finalamt.setText(Constants.convertToArabic(Constants.priceFormat1.format(data.get(invoicePos).getNetamount())));
        custmorename.setText(data.get(invoicePos).getWs().get(0).getUsr().get(0).getUsername());

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    createPDF(data.get(invoicePos).getInvoiceno());
                }
            }
        });

        itemsLayout.removeAllViews();
        for (int i = 0; i < data.get(invoicePos).getWs().get(0).getItems().size(); i++) {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
          View v;
           if (language.equalsIgnoreCase("En")){
               v = inflater.inflate(R.layout.invoice_list, null);
           }
           else {
               v = inflater.inflate(R.layout.invoice_list_ar, null);
           }

            TextView mitem = (TextView) v.findViewById(R.id.item);
            TextView amt = (TextView) v.findViewById(R.id.amt);
            TextView qty = (TextView) v.findViewById(R.id.qty);
            TextView units = (TextView) v.findViewById(R.id.units);

            if(language.equalsIgnoreCase("Ar")){
                mitem.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
                units.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
            }

            qty.setText(""+data.get(invoicePos).getWs().get(0).getItems().get(i).getQuantity());
            if (language.equalsIgnoreCase("En")){
                units.setText(""+data.get(invoicePos).getWs().get(0).getItems().get(i).getMeasuringUnits());
            }
            else {
                units.setText(""+data.get(invoicePos).getWs().get(0).getItems().get(i).getMeasuringUnitsAr());
            }

            mitem.setText(data.get(invoicePos).getWs().get(0).getItems().get(i).getItem());
            amt.setText(Constants.convertToArabic(Constants.priceFormat.format(data.get(invoicePos).getWs().get(0).getItems().get(i).getItemtotal())));
            itemsLayout.addView(v);
        }


        skipRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateLayout.setVisibility(View.GONE);
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(b){
                    Intent intent = new Intent(InvoiceActivity.this, ReviewActivity.class);
                    intent.putExtra("rId", data.get(invoicePos).getRequestid());
                    intent.putExtra("rCode", data.get(invoicePos).getRequestcode());
                    intent.putExtra("distance", data.get(invoicePos).getWs().get(0).getWorkshopdistance());
                    intent.putExtra("wId", data.get(invoicePos).getWorkshopid());
                    if (language.equalsIgnoreCase("En")){
                        intent.putExtra("wNm", data.get(invoicePos).getWs().get(0).getWorkshopnameen());
                    }
                    else {
                        intent.putExtra("wNm", data.get(invoicePos).getWs().get(0).getWorkshopnamear());
                    }

                    intent.putExtra("rating", v);
                    startActivityForResult(intent, 10);
                }
                Log.d(TAG, "wNw"+data.get(0).getWs().get(0).getWorkshopnamear());
                Log.d(TAG, "wNw"+data.get(0).getWs().get(0).getWorkshopnameen());
            }
        });

        if(!data.get(invoicePos).getIsrating()){
            rateLayout.setVisibility(View.VISIBLE);
        }
        else {
            rateLayout.setVisibility(View.GONE);
        }
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
    }

    private void setTypeface(){
        order_number .setTypeface(Constants.getarbooldTypeFace(InvoiceActivity.this));
        date.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        workshopadd.setTypeface(Constants.getarbooldTypeFace(InvoiceActivity.this));
        workshopadd1.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        custmorename.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        totalamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        vatamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        subamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        discountamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        netamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        finalamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        discountamt.setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.order_number_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.date_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.Customer_txt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.amt)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st8)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.st9)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.skip_rating)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));
        ((TextView)findViewById(R.id.history)).setTypeface(Constants.getarTypeFace(InvoiceActivity.this));

    }


    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(InvoiceActivity.this, perm));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
                    createPDF(data.get(invoicePos).getInvoiceno());
                }
                else {
                    Toast.makeText(InvoiceActivity.this, "Storage permission denied, Unable to share invoice", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private void createPDF(String invoice){

        if(data.get(invoicePos).getPdfLocation()!= null) {
            try {
                File file = new File(Environment.getExternalStorageDirectory() + "/samkra/" + invoice + ".pdf");
                if (file.exists()) {
                    sharePdf(invoice);
                } else {
                    new InvoiceActivity.PDFDownload().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(this, "Invoice not found", Toast.LENGTH_SHORT).show();
        }
    }
    // Method for opening a pdf file
    private void sharePdf(String invoice) {
        File outputFile = new File(Environment.getExternalStorageDirectory()+"/samkra/"+invoice+".pdf");
        Uri photoURI = FileProvider.getUriForFile(InvoiceActivity.this,
                getApplicationContext().getPackageName() + ".my.package.name.provider", outputFile);

        Intent share = new Intent();
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, photoURI);

        startActivity(share);
    }
    private class PDFDownload extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "samkra");
                folder.mkdir();
                file12 = new File(folder, data.get(invoicePos).getInvoiceno()+".pdf");
                try {
                    file12.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                FileOutputStream f = new FileOutputStream(file12);

                URL u = new URL(Constants.INVOICE_PATH+data.get(invoicePos).getPdfLocation());
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
//                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d(TAG, "doInBackground: "+buffer.length);
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
                if (language.equalsIgnoreCase("En")){
                    Toast.makeText(InvoiceActivity.this, "Invoice not found", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(InvoiceActivity.this, "لا يوجد فاتورة", Toast.LENGTH_SHORT).show();
                }
            }
            if(loaderDialog!=null) {
                loaderDialog.dismiss();
            }
            sharePdf(data.get(invoicePos).getInvoiceno());
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10 && resultCode == RESULT_OK){
            rateLayout.setVisibility(View.GONE);
        }
        else {
            rateLayout.setVisibility(View.VISIBLE);
            ratingBar.setRating(0);
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InvoiceActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
