package com.cs.samkracustomer.Fragements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Activity.VehicleImagesActivity;
import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Activity.SignInActivity;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.samkracustomer.Activity.ForgotPasswordActivity.TAG;

public class HomeFragment extends Fragment implements OnMapReadyCallback {

    ImageView captureIcon,menu_btn;
    ImageView homecamra, recipits, inbox, notification;
    View rootView;
    SharedPreferences userPrefs;
    private static final int INBOX = 1;
    private static final int HISTORY = 2;
    private static final int NOTIFICATIONS = 3;
    private GoogleMap mMap;
    SharedPreferences.Editor userPrefsEditor;
    TextView dlanguage,bidcount;
    String language;
    String userId;
    AlertDialog loaderDialog;
    ArrayList<GetUserNewRequestResponse.Data> data = new ArrayList<>();
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            rootView = inflater.inflate(R.layout.activity_home, container, false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.activity_home_ar, container, false);
        }

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        menu_btn = (ImageView) rootView.findViewById(R.id.meu_btn);
        captureIcon =(ImageView) rootView.findViewById(R.id.capture_icon);
        recipits =(ImageView) rootView.findViewById(R.id.recipts);
        inbox =(ImageView) rootView.findViewById(R.id.inbox);
        notification =(ImageView) rootView.findViewById(R.id.home_notificaton);
        bidcount =(TextView) rootView.findViewById(R.id.bid_count);
        dlanguage = rootView.findViewById(R.id.language);


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        captureIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VehicleImagesActivity.class);
                startActivity(intent);
            }
        });

        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userPrefs.getString("userId","").equals("")) {
//                    Intent intent = new Intent(getActivity(), InboxFragment.class);
//                    startActivity(intent);
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment inboxFragment = new InboxFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxFragment).commit();
                }
                else {
                    Intent intent = new Intent(getActivity(), SignInActivity.class);
                    startActivityForResult(intent, INBOX);
                }
            }
        });
        dlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    getActivity().finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("type", "1");
                    startActivity(i);
                    getActivity().finish();
                }
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userPrefs.getString("userId","").equals("")) {
//                    Intent intent = new Intent(getActivity(), NotificationFragment.class);
//                    startActivity(intent);
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment notificationFragment = new NotificationFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationFragment).commit();
                }
                else{
                    Intent intent = new Intent(getActivity(), SignInActivity.class);
                    startActivityForResult(intent, NOTIFICATIONS);
                }
            }
        });
        recipits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userPrefs.getString("userId","").equals("")) {
//                    Intent intent = new Intent(getActivity(), HistoryFragment.class);
//                    startActivity(intent);
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment historyFragment = new HistoryFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, historyFragment).commit();
                }
                else{
                    Intent intent = new Intent(getActivity(), SignInActivity.class);
                    startActivityForResult(intent, HISTORY);
                }
            }
        });
        userId = userPrefs.getString("userId", null);
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
        return rootView;
    }
    private void setTypeface(){

        ((TextView)rootView.findViewById(R.id.st1)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st2)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st3)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st4)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st5)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st6)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st7)).setTypeface(Constants.getarTypeFace(getActivity()));
        ((TextView)rootView.findViewById(R.id.st8)).setTypeface(Constants.getarTypeFace(getActivity()));

    }

    @Override
    public void onResume() {
        super.onResume();
        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new InboxApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class InboxApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<GetUserNewRequestResponse> call = apiService.GetUserNewRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetUserNewRequestResponse>() {
                @Override
                public void onResponse(Call<GetUserNewRequestResponse> call, Response<GetUserNewRequestResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        GetUserNewRequestResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if (data.size() == 0) {
                                    bidcount.setVisibility(View.GONE);
                                }
                                else {
                                    bidcount.setText("" + data.size());
                                    bidcount.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<GetUserNewRequestResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                }

            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(24.810936, 46.648461);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                sydney, 12);
        mMap.animateCamera(location);


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == INBOX && resultCode == RESULT_OK){
            FragmentManager fragmentManager = getFragmentManager();
            Fragment inboxFragment = new InboxFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxFragment).commit();
        }
        else if(requestCode == HISTORY && resultCode == RESULT_OK){
            FragmentManager fragmentManager = getFragmentManager();
            Fragment historyFragment = new HistoryFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, historyFragment).commit();
        }
        else if(requestCode == NOTIFICATIONS && resultCode == RESULT_OK){
            FragmentManager fragmentManager = getFragmentManager();
            Fragment notificationFragment = new NotificationFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationFragment).commit();
        }
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }
}