package com.cs.samkracustomer.Fragements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Activity.InvoiceActivity;
import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Adapters.HistoryAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {


    ImageView menu_btn,testcount;
//    TextView invoicenumber,offernumber;
    ListView requestsList;
    HistoryAdapter mAdapter;
    ArrayList<AccpetedOffersResponce.Data> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    View rootView;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    TextView offernumber;
    SharedPreferences.Editor userPrefsEditor;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    AlertDialog loaderDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            rootView = inflater.inflate(R.layout.historory_layout, container, false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.historory_layout_ar, container, false);
        }



        menu_btn=(ImageView)rootView.findViewById(R.id.hy_back_btn);
        testcount=(ImageView)rootView.findViewById(R.id.testcount);
//        invoicenumber=(TextView)findViewById(R.id.hy_invoicenumber);
//        offernumber=(TextView)findViewById(R.id.hy_accpetnumber);
        requestsList=(ListView)rootView. findViewById(R.id.hy_listview);
        offernumber=(TextView)rootView.findViewById(R.id.n_bid_count);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        requestsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), InvoiceActivity.class);
                intent.putExtra("data", data);
                intent.putExtra("pos",i);
                startActivity(intent);
            }
        });
        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });
        testcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }
  return rootView;
    }
    private void setTypeface(){

        ((TextView)rootView.findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(getActivity()));

    }

    @Override
    public void onResume() {
        super.onResume();
        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new userRequestsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

    }

    private class  userRequestsApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();

        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AccpetedOffersResponce> call = apiService.historylist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AccpetedOffersResponce>() {
                @Override
                public void onResponse(Call<AccpetedOffersResponce> call, Response<AccpetedOffersResponce> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        AccpetedOffersResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    Constants.showOneButtonAlertDialog("No history found", getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                mAdapter = new HistoryAdapter(getActivity(), data, Language);
                                requestsList.setAdapter(mAdapter);
                                offernumber.setText(""+data.size());
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<AccpetedOffersResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

}