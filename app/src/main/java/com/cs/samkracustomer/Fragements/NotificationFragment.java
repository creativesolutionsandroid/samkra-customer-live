package com.cs.samkracustomer.Fragements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.samkracustomer.Activity.FinalPrice_Bid;
import com.cs.samkracustomer.Activity.InvoiceActivity;
import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Activity.OfferDetailsActivity;
import com.cs.samkracustomer.Adapters.NotificationAdapter;
import com.cs.samkracustomer.Models.AccpetedOffersResponce;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.NotificationResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {

    ImageView  menu_btn,textcount;
    TextView offernumber;
    ListView requestsList;
    NotificationAdapter mAdapter;
    ArrayList<NotificationResponce.Data> data = new ArrayList<>();
    ArrayList<GetUserNewRequestResponse.Data> inboxdata = new ArrayList<>();
    ArrayList<AccpetedOffersResponce.Data> historyData = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    View rootView;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    String reqid;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    AlertDialog loaderDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs =getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            rootView = inflater.inflate(R.layout.notification_layout, container, false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.notification_layout_ar, container, false);
        }

        menu_btn=(ImageView)rootView.findViewById(R.id.n_back_btn);
        offernumber=(TextView)rootView.findViewById(R.id.n_bid_count);
        requestsList=(ListView) rootView.findViewById(R.id.notification_list);
        textcount=(ImageView)rootView.findViewById(R.id.count);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new userRequestsApi().execute();
        }
        else{
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }


        requestsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                reqid = data.get(i).getReqid();
                if (data.get(i).getLatestpnttype().equals("2")){
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment inboxFragment = new InboxFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxFragment).commit();
                }
                else if (data.get(i).getLatestpnttype().equals("3")||
                        data.get(i).getLatestpnttype().equals("4")||
                        data.get(i).getLatestpnttype().equals("5")||
                        data.get(i).getLatestpnttype().equals("6")){
                    new InboxApi().execute();
                }
                else if (data.get(i).getLatestpnttype().equals("7")){
                    new invoiceApi().execute();
                }
            }
        });
        textcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });
        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

        return rootView;
    }
    private void setTypeface(){

        ((TextView)rootView.findViewById(R.id.st1)).setTypeface(Constants.getarTypeFace(getActivity()));

    }

    private class  userRequestsApi extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            showloaderAlertDialog();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<NotificationResponce> call = apiService.Notification(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<NotificationResponce>() {
                @Override
                public void onResponse(Call<NotificationResponce> call, Response<NotificationResponce> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        NotificationResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                data = verifyMobileResponse.getData();
                                if(data.size() == 0){
                                    if (language.equalsIgnoreCase("En")){
                                        Constants.showOneButtonAlertDialog("No notifications found", getResources().getString(R.string.app_name),
                                                getResources().getString(R.string.ok), getActivity());
                                    }
                                    else {
                                        Constants.showOneButtonAlertDialog("لا إشعار أسعار", getResources().getString(R.string.app_name_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    }

                                }
                                mAdapter = new NotificationAdapter(getActivity(), data, language);
                                requestsList.setAdapter(mAdapter);
                                offernumber.setText(""+data.size());
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                }
                            else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<NotificationResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
                parentObj.put("Usertype","1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }
    private class InboxApi extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
         showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<GetUserNewRequestResponse> call = apiService.GetUserNewRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetUserNewRequestResponse>() {
                @Override
                public void onResponse(Call<GetUserNewRequestResponse> call, Response<GetUserNewRequestResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        GetUserNewRequestResponse verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                inboxdata = verifyMobileResponse.getData();
                                if (data.size() == 0) {
                                    if (language.equalsIgnoreCase("En")) {
                                        Constants.showOneButtonAlertDialog("Bid not found", getResources().getString(R.string.app_name),
                                                getResources().getString(R.string.ok), getActivity());
                                    } else {
                                        Constants.showOneButtonAlertDialog("المزايدة غير موجودة", getResources().getString(R.string.app_name_ar),
                                                getResources().getString(R.string.ok_ar), getActivity());
                                    }
                                }
                                else{
                                    if(inboxdata.get(0).getRequeststatus().equalsIgnoreCase("pending")){
                                        Intent intent = new Intent(getActivity(), OfferDetailsActivity.class);
                                        intent.putExtra("id", ""+inboxdata.get(0).getRequestid());
                                        intent.putExtra("carData", inboxdata);
                                        intent.putExtra("carPos",0);
                                        getActivity().startActivity(intent);
                                    }
                                    else if(inboxdata.get(0).getRequeststatus().equalsIgnoreCase("Accepted Final Price")){
                                        Intent intent = new Intent(getActivity(), FinalPrice_Bid.class);
                                        intent.putExtra("data", inboxdata);
                                        intent.putExtra("bidPos",0);
                                        getActivity().startActivity(intent);
                                    }
                                    else {
                                        Intent intent = new Intent(getActivity(), FinalPrice_Bid.class);
                                        intent.putExtra("data", inboxdata);
                                        intent.putExtra("bidPos",0);
                                        getActivity().startActivity(intent);
                                    }
                                }

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<GetUserNewRequestResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
                parentObj.put("RequestId", reqid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private class invoiceApi extends AsyncTask<String, String, String> {


        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
         showloaderAlertDialog();
        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AccpetedOffersResponce> call = apiService.historylist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AccpetedOffersResponce>() {
                @Override
                public void onResponse(Call<AccpetedOffersResponce> call, Response<AccpetedOffersResponce> response) {
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response);
                        AccpetedOffersResponce verifyMobileResponse = response.body();
                        try {
                            if (verifyMobileResponse.getStatus()) {
                                historyData = verifyMobileResponse.getData();
                                Intent intent = new Intent(getActivity(), InvoiceActivity.class);
                                intent.putExtra("data", historyData);
                                intent.putExtra("pos",0);
                                startActivity(intent);
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), getActivity());

                                }
                                else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<AccpetedOffersResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }
        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("UserId", userId);
                parentObj.put("RequestId", reqid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

}
