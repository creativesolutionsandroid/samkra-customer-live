package com.cs.samkracustomer.Fragements;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Activity.ChangePassword;
import com.cs.samkracustomer.Activity.MainActivity;
import com.cs.samkracustomer.Activity.SplashScreen;
import com.cs.samkracustomer.Models.Signupresponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Rest.APIInterface;
import com.cs.samkracustomer.Rest.ApiClient;
import com.cs.samkracustomer.Utils.Constants;
import com.cs.samkracustomer.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;


import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.samkracustomer.Activity.MainActivity.profilePic;

public class MyProfileFragment extends Fragment {

    TextView Signup;
    private static final int CAMERA_REQUEST = 1888;

    AlertDialog customDialog;
    ImageView edit_user,edit_email,edit_password;
    Button Upassword;
    Button Update;
    String strMobile, strPassword, strEmail, strUsername,strUserid;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    public static boolean isOTPVerified = false;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;
    EditText inputPassword, inputName, inputEmail, inputMobile;
    ImageView  menu_btn,editprofile;
    CircleImageView profilepic;
    View rootView;
    AlertDialog loaderDialog;

    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    Bitmap thumbnail;
    Boolean isImageChanged = false;
    String pictureName = "";
    String imageResponse;
    private DefaultHttpClient mHttpClient11;
    SharedPreferences.Editor languagePrefsEditor;
    Uri imageUri;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            rootView = inflater.inflate(R.layout.account_fragment, container, false);
        }else if(language.equalsIgnoreCase("Ar")){
            rootView = inflater.inflate(R.layout.account_fragment_ar, container, false);
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        menu_btn = (ImageView) rootView.findViewById(R.id.back_btn);
        profilepic = (CircleImageView)rootView. findViewById(R.id.ac_profilrpic);
        editprofile = (ImageView) rootView.findViewById(R.id.ac_edit);
        inputName = (EditText) rootView.findViewById(R.id.username);
        inputEmail = (EditText)rootView. findViewById(R.id.email);
        inputMobile = (EditText) rootView.findViewById(R.id.phone);
        inputPassword = (EditText) rootView.findViewById(R.id.password);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
        strUserid = userPrefs.getString("userId", null);
        Update =(Button)rootView.findViewById(R.id.update);
        edit_user=(ImageView)rootView.findViewById(R.id.edit_user);
        edit_email=(ImageView)rootView.findViewById(R.id.edit_email);
        edit_password=(ImageView)rootView.findViewById(R.id.edit_password);

        inputName.setText( userPrefs.getString("name", null));
        inputMobile.setText("+" +  userPrefs.getString("mobile", null));
        inputEmail.setText( userPrefs.getString("email", null));

        strMobile = userPrefs.getString("mobile", "");
        pictureName = userPrefs.getString("pic", "");

        if(!userPrefs.getString("pic","").equalsIgnoreCase("")) {
            Glide.with(MyProfileFragment.this)
                    .load(Constants.IMAGE_URL + userPrefs.getString("pic", ""))
                    .into(profilepic);
        }

        edit_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputName.setEnabled(true);
            }
        });
        edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputPassword.setEnabled(true);
            }
        });
        edit_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputEmail.setEnabled(true);
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });
        Update.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (validations()) {
                    if(isImageChanged){
                        new uploadImagesApi().execute();
                    }
                    else {
                        new updateProfileApi().execute();
                    }
                }
            }
        });

        edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                startActivity(intent);
            }
        });
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });

        if (language.equalsIgnoreCase("Ar")){
            setTypeface();
        }

        return rootView;
    }
    private void setTypeface(){

        ((TextView)rootView.findViewById(R.id.tv1)).setTypeface(Constants.getarTypeFace(getActivity()));
        inputPassword .setTypeface(Constants.getarTypeFace(getActivity()));
        inputName .setTypeface(Constants.getarTypeFace(getActivity()));
        inputEmail .setTypeface(Constants.getarTypeFace(getActivity()));
        inputMobile .setTypeface(Constants.getarTypeFace(getActivity()));
        Update .setTypeface(Constants.getarTypeFace(getActivity()));

    }
    private boolean validations() {
        strUsername = inputName.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        if (strUsername.length() == 0) {
            if (language.equalsIgnoreCase("En")){
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            }
            else {
                inputName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }

            return false;
        } else if (strEmail.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            }
            else {
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));

            }
            return false;
        } else if (!isValidEmail(strEmail)) {
            if (language.equalsIgnoreCase("En")){
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            }
            else {
                inputEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }

            return false;
        }
        return true;
    }

    public boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }




    @Override
    public void onResume() {
        super.onResume();

        inputName.setText( userPrefs.getString("name", null));
        inputMobile.setText("+" +  userPrefs.getString("mobile", null));
        inputEmail.setText( userPrefs.getString("email", null));
    }


    private class updateProfileApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLoginJson();
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.updateProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            Log.d("TAG", "onResponse: " + userId);
                            userPrefEditor.putString("userId", userId);
                            userPrefEditor.putString("name", registrationResponse.getData().getUsername());
                            userPrefEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefEditor.putString("pic", pictureName);
                            userPrefEditor.commit();
                            inputName.setText( userPrefs.getString("name", null));
                            inputMobile.setText("+" +  userPrefs.getString("mobile", null));
                            inputEmail.setText(userPrefs.getString("email", null));
                            inputName.setEnabled(false);
                            inputEmail.setEnabled(false);
                            if(!userPrefs.getString("pic", "").equalsIgnoreCase("")) {
                                Glide.with(getContext())
                                        .load(Constants.IMAGE_URL + userPrefs.getString("pic", ""))
                                        .into(profilePic);
                            }
                            else {
                                profilePic.setImageResource(R.drawable.main);
                            }
                            if (loaderDialog != null) {
                                loaderDialog.dismiss();
                            }
                        } else {
//                          status false case
                            if (language.equalsIgnoreCase("En")){
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                            else {
                                String failureResponse = registrationResponse.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }

                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (loaderDialog != null) {
                        loaderDialog.dismiss();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (loaderDialog != null) {
                loaderDialog.dismiss();
            }
        }


        private String prepareLoginJson() {
            JSONObject parentObj = new JSONObject();

            try {
                parentObj.put("Phone", strMobile);
                parentObj.put("Email", strEmail);
                parentObj.put("UserId", strUserid);
                parentObj.put("ProfilePhoto", pictureName);
                parentObj.put("Username", strUsername);
                parentObj.put("DeviceToken", SplashScreen.regId);
                parentObj.put("Language", language);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareLoginJson: "+parentObj.toString());
            return parentObj.toString();
        }
    }

    public void showtwoButtonsAlertDialog () {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog_camera;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel =(TextView) dialogView.findViewById(R.id.cancel_btn);
     if (language.equalsIgnoreCase("En")){
        title.setText("Samkra");
        no.setText("Gallery");
        yes.setText("Camera");
        desc.setText("Pick one from the options");
        cancel.setText("Cancel");
     }
     else {
         title.setText("سمكره");
         no.setText("إستديو الصور");
         yes.setText("كاميرا");
         desc.setText("اختار من الخيارات التالية");
         cancel.setText("الغاء");

     }

        if (language.equalsIgnoreCase("Ar")){
            title.setTypeface(Constants.getarTypeFace(getContext()));
            desc.setTypeface(Constants.getarTypeFace(getContext()));
            yes.setTypeface(Constants.getarTypeFace(getContext()));
            no.setTypeface(Constants.getarTypeFace(getContext()));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                customDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera=false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if(language.equalsIgnoreCase("En")){
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
                }
                else{
                    startActivityForResult(Intent.createChooser(intent, "اختر الصورة"), PICK_IMAGE_FROM_GALLERY);

                }
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            if (language.equalsIgnoreCase("En")) {
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
            } else {
                startActivityForResult(Intent.createChooser(intent, "اختر الصورة"), PICK_IMAGE_FROM_GALLERY);

            }
        }
    }


    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera=true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getActivity().getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, CAMERA_REQUEST);
            }
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
//            thumbnail = (Bitmap) data.getExtras().get("data");
            convertCameraImage();
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            convertGalleryImage();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void convertCameraImage() {
        String path = getRealPathFromURI(imageUri);

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inSampleSize = 8;
        bmOptions.inScaled = false;

        thumbnail = BitmapFactory.decodeFile(path, bmOptions);

        Matrix matrix = new Matrix();

        matrix.postRotate(90);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, 900,
                800, true);

        thumbnail = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        Log.d("TAG", "getByteCount: "+thumbnail.getByteCount());

        profilepic.setImageBitmap(thumbnail);
        isImageChanged = true;
    }


    private void convertGalleryImage() {
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 900, 800,
                false);
        thumbnail = codec(thumbnail, Bitmap.CompressFormat.JPEG, 100);
        Log.d("TAG", "convertGalleryImage: "+thumbnail.getByteCount());
        profilepic.setImageBitmap(thumbnail);
        isImageChanged = true;
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    private class uploadImagesApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... str) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());

            HttpResponse httpResponse = null;
            int count = 0, usedCount = 0;

            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            mHttpClient11 = new DefaultHttpClient(params);
            HttpPost httppost = new HttpPost(Constants.IMAGE_UPLOAD_URL);

            try{
                String imagePath1 = "";
                File file=null;
                if (thumbnail != null) {
                    imagePath1 = StoreByteImage(thumbnail, "back");
                }
                if (imagePath1 != null) {
                    file = new File(imagePath1);
                }
                if (null != file && file.exists()) {
                    multipartEntity.addPart("image", new FileBody(file));
                }
                httppost.setEntity(multipartEntity);
                httpResponse=  mHttpClient11.execute(httppost);
            }catch(Exception e){
                e.printStackTrace();
            }

            InputStream entity = null;
            try {
                entity = httpResponse.getEntity().getContent();
                if(entity != null) {
                    imageResponse = convertInputStreamToString(entity);
                    Log.i("TAG", "user response:" + imageResponse);
                }
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO Auto-generated method stub
            super.onPostExecute(s);
            if(imageResponse!=null && imageResponse.length()>0) {
                try {
                    JSONArray resultArray = new JSONArray(imageResponse);
                    new updateProfileApi().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                if(language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), "Images upload failed.Please try again", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(), "خطأ في تحميل الصور ، الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
            if(loaderDialog!=null){
                loaderDialog.dismiss();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        pictureName = "PIC_" + strUserid + "_" + timeStamp + ".jpg";

        File sdImageMainDirectory = new File("/sdcard/"+ pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options=new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.JPEG,100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }
    public void showloaderAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loder_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

//       ImageView imageView =(ImageView)findViewById(R.id.loaderimage);
//        Glide.with(SignupActivity.this)
//                .load(getResources().getDrawable(R.raw.loading2x)).asGif()
//                .crossFade()
//                .into(imageView);
        loaderDialog = dialogBuilder.create();
        loaderDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loaderDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;

//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

}