package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SaveUserRequestResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("MessageEn")
    private String message;
    @Expose
    @SerializedName("MessageAr")
    private String messageAr;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public String getMessageAr() {
        return messageAr;
    }

    public void setMessageAr(String messageAr) {
        this.messageAr = messageAr;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("RequestId")
        private int requestid;

        @Expose
        @SerializedName("RequestCode")
        private String requestCode;

        public String getRequestCode() {
            return requestCode;
        }

        public void setRequestCode(String requestCode) {
            this.requestCode = requestCode;
        }

        public int getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }
    }
}
