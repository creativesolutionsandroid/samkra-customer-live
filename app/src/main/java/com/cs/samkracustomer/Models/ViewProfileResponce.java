package com.cs.samkracustomer.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public  class ViewProfileResponce implements Serializable {
    @Expose
    @SerializedName("Data")
    private ArrayList<Data> data;
    @Expose
    @SerializedName("MessageEn")
    private String message;
    @Expose
    @SerializedName("MessageAr")
    private String messageAr;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public String getMessageAr() {
        return messageAr;
    }

    public void setMessageAr(String messageAr) {
        this.messageAr = messageAr;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("wr")
        private ArrayList<Wr> wr;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Logo")
        private String logo;
        @Expose
        @SerializedName("WorkshopId")
        private int workshopid;

        @SerializedName("AboutEn")
        private String AboutEn;

        public String getAboutEn() {
            return AboutEn;
        }

        public void setAboutEn(String aboutEn) {
            AboutEn = aboutEn;
        }

        public String getAboutAr() {
            return AboutAr;
        }

        public void setAboutAr(String aboutAr) {
            AboutAr = aboutAr;
        }

        @SerializedName("AboutAr")
        private String AboutAr;



        public ArrayList<Wr> getWr() {
            return wr;
        }

        public void setWr(ArrayList<Wr> wr) {
            this.wr = wr;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public int getWorkshopid() {
            return workshopid;
        }

        public void setWorkshopid(int workshopid) {
            this.workshopid = workshopid;
        }
    }

    public static class Wr implements Serializable{
        @Expose
        @SerializedName("CreatedDate")
        private String createddate;
        @Expose
        @SerializedName("Comments")
        private String comments;
        @Expose
        @SerializedName("Rating")
        private float rating;
        @Expose
        @SerializedName("RequestId")
        private int requestid;
        @Expose
        @SerializedName("Username")
        private String username;
        @Expose
        @SerializedName("UserId")
        private int userid;

        public String getCreateddate() {
            return createddate;
        }

        public void setCreateddate(String createddate) {
            this.createddate = createddate;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public float getRequestid() {
            return requestid;
        }

        public void setRequestid(int requestid) {
            this.requestid = requestid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }
    }
}
