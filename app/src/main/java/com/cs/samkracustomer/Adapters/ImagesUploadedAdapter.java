package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cs.samkracustomer.Activity.VehicleImagesActivity;
import com.cs.samkracustomer.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class ImagesUploadedAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<Bitmap> images;
    Context context;
    String language;

    public ImagesUploadedAdapter(Context context, ArrayList<Bitmap> images, String language) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
        this.language = language;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 3.2f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_image, container, false);

        ImageView store_image = (ImageView) itemView.findViewById(R.id.image);
        store_image.setImageBitmap(images.get(position));

        if (language.equalsIgnoreCase("Ar")){
            itemView.setRotationY(180);
        }

        store_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                VehicleImagesActivity.imageView.setImageBitmap(getBitmap(images.get(position)));
                VehicleImagesActivity.imageView.setImageBitmap(images.get(position));
                VehicleImagesActivity.selectedBitmap = images.get(position);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    private Bitmap getBitmap(File file){
        Bitmap bitmap = null;
        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);

            Matrix matrix = new Matrix();
            if (rotation != 0) {
                matrix.preRotate(rotationInDegrees);
            }

            Bitmap sourceBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            bitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}