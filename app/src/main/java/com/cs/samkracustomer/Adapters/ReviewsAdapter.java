package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Activity.ProfileActivity;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.Models.OffersResponse;
import com.cs.samkracustomer.Models.ViewProfileResponce;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ReviewsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<ViewProfileResponce.Wr> historyArrayList = new ArrayList<>();

    String language;
    SharedPreferences languagePrefs;



    public ReviewsAdapter(Context context, ArrayList<ViewProfileResponce.Wr> orderList, String language) {
        this.context = context;
        this.historyArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return historyArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        ArrayList<OffersResponse.Data> historyArrayList = new ArrayList<>();
        String language;

        TextView workshopname,wrokshopabout,wrokshopdate;
        RatingBar ratingBar;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.profile_child, null);
            }
            else{
                convertView = inflater.inflate(R.layout.profile_child_ar, null);
            }

            holder.workshopname = (TextView) convertView.findViewById(R.id.pf_name);
            holder.wrokshopabout = (TextView) convertView.findViewById(R.id.pf_about);
            holder.wrokshopdate = (TextView) convertView.findViewById(R.id.pf_date);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.pf_ratingbar);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.workshopname.setText(historyArrayList.get(position).getUsername());
        holder.wrokshopabout.setText(historyArrayList.get(position).getComments());
//        holder.wrokshopdate.setText(historyArrayList.get(position).getCreateddate());

        String str = historyArrayList.get(position).getCreateddate();
        String[] array = str.split(" ");
        holder.wrokshopdate.setText(Constants.convertToArabic(array[0]));
        try {
            holder.ratingBar.setRating((historyArrayList.get(position).getRating()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (language.equalsIgnoreCase("Ar")){
            holder.workshopname.setTypeface(Constants.getarTypeFace(context));
            holder.wrokshopabout.setTypeface(Constants.getarTypeFace(context));
            holder.wrokshopdate.setTypeface(Constants.getarTypeFace(context));
        }

        return convertView;
    }
}