package com.cs.samkracustomer.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.samkracustomer.Activity.FinalPrice_Bid;
import com.cs.samkracustomer.Activity.OfferDetailsActivity;
import com.cs.samkracustomer.Activity.OffersActivity;
import com.cs.samkracustomer.Models.GetUserNewRequestResponse;
import com.cs.samkracustomer.R;
import com.cs.samkracustomer.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class InboxListAdapter extends BaseAdapter{

    ProgressBar progressBar;
    public Context context;
    public LayoutInflater inflater;
    ArrayList<GetUserNewRequestResponse.Data> inboxArrayList = new ArrayList<>();
    ImageView  back_dot,front_dot,left_dot,right_dot;
    ViewPageAdapter viewPageAdapter;
    int type;
    String language;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    public InboxListAdapter(Context context, ArrayList<GetUserNewRequestResponse.Data> orderList, String language) {
        this.context = context;
        this.inboxArrayList = orderList;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return inboxArrayList.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView mrequest_code, mdate, mcar_right, mcar_left, mcar_front, mcar_back, car_make, car_model, car_year, expiresInTitle, expiresIn, bids_count, min_price, max_price, final_price,numberofbid,st1,st2,st3,st4,st5,st6,st7,st8,st9;
        LinearLayout mprice_layout, back_layout, front_layout, left_layout, right_layout, bidsLayout, titles, expiresInLayout;
        RelativeLayout mbids_count_layout;
        ViewPager img_list;
        ImageView back_side, front_side, left_side, right_side, dropdown;
        LinearLayout imagesLayout;
        ImageView  right_dot,left_dot,front_dot,back_dot;
        Button button;
        RelativeLayout mainLayout;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            languagePrefs =context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            languagePrefsEditor  = languagePrefs.edit();
            language = languagePrefs.getString("language", "En");
            if(language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.inbox_list, null);
            }
            else{
                convertView = inflater.inflate(R.layout.inbox_list_ar, null);
            }

            holder.mrequest_code = (TextView) convertView.findViewById(R.id.request_code);
            holder.mdate = (TextView) convertView.findViewById(R.id.date);
            holder.mcar_back = (TextView) convertView.findViewById(R.id.car_back);
            holder.mcar_left = (TextView) convertView.findViewById(R.id.car_left);
            holder.mcar_right = (TextView) convertView.findViewById(R.id.car_right);
            holder.mcar_front = (TextView) convertView.findViewById(R.id.car_front);
            holder.car_make = (TextView) convertView.findViewById(R.id.car_make);
            holder.car_model = (TextView) convertView.findViewById(R.id.car_model);
            holder.car_year = (TextView) convertView.findViewById(R.id.car_year);
            holder.bids_count = (TextView) convertView.findViewById(R.id.bids_count);
            holder.min_price = (TextView) convertView.findViewById(R.id.min_price);
            holder.max_price = (TextView) convertView.findViewById(R.id.max_price);
            holder.final_price = (TextView) convertView.findViewById(R.id.final_price);
            holder.expiresIn = (TextView) convertView.findViewById(R.id.expires_in_);
            holder.expiresInTitle = (TextView) convertView.findViewById(R.id.expires_in_title);
            holder.button = (Button) convertView.findViewById(R.id.button);
            holder.numberofbid =(TextView) convertView.findViewById(R.id.numberofb);

            holder.mbids_count_layout = (RelativeLayout) convertView.findViewById(R.id.bids_count_layout);
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.layout1);

            holder.expiresInLayout = (LinearLayout) convertView.findViewById(R.id.expires_in_layout);
            holder.mprice_layout = (LinearLayout) convertView.findViewById(R.id.price_layount);
            holder.back_layout = (LinearLayout) convertView.findViewById(R.id.back_layout);
            holder.titles = (LinearLayout) convertView.findViewById(R.id.titles);
            holder.left_layout = (LinearLayout) convertView.findViewById(R.id.left_layout);
            holder.right_layout = (LinearLayout) convertView.findViewById(R.id.right_layout);
            holder.front_layout = (LinearLayout) convertView.findViewById(R.id.front_layout);
            holder.bidsLayout = (LinearLayout) convertView.findViewById(R.id.bids_layout);

            holder.img_list = (ViewPager) convertView.findViewById(R.id.view_pager);

            holder.back_side = (ImageView) convertView.findViewById(R.id.back_side);
            holder.front_side = (ImageView) convertView.findViewById(R.id.front_side);
            holder.left_side = (ImageView) convertView.findViewById(R.id.left_side);
            holder.right_side = (ImageView) convertView.findViewById(R.id.right_side);
            holder.dropdown = (ImageView) convertView.findViewById(R.id.down);
            holder.imagesLayout = (LinearLayout) convertView.findViewById(R.id.images_layout);

            holder.st1=(TextView) convertView.findViewById(R.id.st1);
            holder.st2=(TextView) convertView.findViewById(R.id.st2);
            holder.st3=(TextView) convertView.findViewById(R.id.st3);
            holder.st4=(TextView) convertView.findViewById(R.id.st4);
            holder.st5=(TextView) convertView.findViewById(R.id.st5);
            holder.st6=(TextView) convertView.findViewById(R.id.st6);
            holder.st7=(TextView) convertView.findViewById(R.id.st7);
            holder.st8=(TextView) convertView.findViewById(R.id.st8);
            holder.st9=(TextView) convertView.findViewById(R.id.st9);
            holder.back_dot = (ImageView) convertView.findViewById(R.id.back_dot);
            holder.front_dot = (ImageView) convertView.findViewById(R.id.front_dot);
            holder.left_dot = (ImageView) convertView.findViewById(R.id.left_dot);
            holder.right_dot = (ImageView) convertView.findViewById(R.id.right_dot);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("Ar")){
            holder.mrequest_code.setTypeface(Constants.getarbooldTypeFace(context));
            holder.mdate.setTypeface(Constants.getarbooldTypeFace(context));
            holder.mcar_right.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_left.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_front.setTypeface(Constants.getarTypeFace(context));
            holder.mcar_back.setTypeface(Constants.getarTypeFace(context));
            holder.car_make.setTypeface(Constants.getarbooldTypeFace(context));
            holder.car_model.setTypeface(Constants.getarbooldTypeFace(context));
            holder.car_year.setTypeface(Constants.getarbooldTypeFace(context));
            holder.numberofbid.setTypeface(Constants.getarTypeFace(context));
            holder.max_price.setTypeface(Constants.getarbooldTypeFace(context));
            holder.min_price.setTypeface(Constants.getarbooldTypeFace(context));
            holder.final_price.setTypeface(Constants.getarbooldTypeFace(context));
            holder.st1.setTypeface(Constants.getarTypeFace(context));
            holder.st2.setTypeface(Constants.getarTypeFace(context));
            holder.st3.setTypeface(Constants.getarTypeFace(context));
            holder.st4.setTypeface(Constants.getarTypeFace(context));
            holder.st5.setTypeface(Constants.getarTypeFace(context));
            holder.st6.setTypeface(Constants.getarTypeFace(context));
            holder.st7.setTypeface(Constants.getarTypeFace(context));
            holder.st8.setTypeface(Constants.getarTypeFace(context));
            holder.st9.setTypeface(Constants.getarTypeFace(context));
            holder.button.setTypeface(Constants.getarTypeFace(context));

        }

        holder.mrequest_code.setText("#"+inboxArrayList.get(position).getRequestcode());

        String date = inboxArrayList.get(position).getCreatedon();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.mdate.setText(""+Constants.convertToArabic(date));

        if (language.equalsIgnoreCase("En")){

            holder.car_make.setText(""+inboxArrayList.get(position).getCm().get(0).getCarmakernameen());
            holder.car_model.setText(""+inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getModelnameen());
            holder.car_year.setText(""+inboxArrayList.get(position).getYear());
        }
        else {


        holder.car_make.setText(""+inboxArrayList.get(position).getCm().get(0).getCarmakernamear());
        holder.car_model.setText(""+inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getModelnamear());
        holder.car_year.setText(""+inboxArrayList.get(position).getYear());
        }
        ArrayList<String> images = new ArrayList<>();
        boolean isRight = false;
        boolean isLeft = false;
        boolean isFront = false;
        boolean isBack = false;
        for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
            if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 1) {
                isRight = true;
            }
            if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 3) {
                isLeft = true;
            }
            if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 2) {
                isFront = true;
            }
            if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 4) {
                isBack = true;
            }
        }
        if(!isRight){
            holder.right_layout.setEnabled(false);
            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_right));
            holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.right_layout.setEnabled(true);
            holder.right_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_right_blue));
            holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
            for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 1) {
                    images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                }
                else {
                }
            }
        }
        if(!isLeft){
            holder.left_layout.setEnabled(false);
            holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_left));
            holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.left_layout.setEnabled(true);
            holder.left_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_left_selected));
            if(!isRight) {
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 3) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    } else {
                    }
                }
            }
            else{
            }
        }
        if(!isBack){
            holder.back_layout.setEnabled(false);
            holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_new_unselected));
            holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.back_layout.setEnabled(true);
            holder.back_side.setImageDrawable(context.getResources().getDrawable(R.drawable.back_new_selected));
            if(!isRight && !isLeft && !isFront) {
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(0).getDocumenttype() == 4) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    } else {
                    }
                }
            }
            else{
            }
        }
        if(!isFront){
            holder.front_layout.setEnabled(false);
            holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front));
            holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
        }
        else{
            holder.front_layout.setEnabled(true);
            holder.front_side.setImageDrawable(context.getResources().getDrawable(R.drawable.car_front_selected));
            if(!isRight && !isLeft) {
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 2) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    } else {
                    }
                }
            }
            else{
            }
        }
//        for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
//            images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
//        }
        holder.imagesLayout.removeAllViews();
        for (int i = 0; i < images.size(); i++) {
            View v = inflater.inflate(R.layout.list_image, null);
            ImageView imageView = v.findViewById(R.id.image);
            Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
            holder.imagesLayout.addView(v);
        }
        holder.right_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 1) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    }
                    else {
                    }
                }
                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });
        holder.left_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 3) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    }
                    else {
                    }
                }
                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });
        holder.front_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 2) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    }
                    else {
                    }
                }
                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });
        holder.back_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.right_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.left_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.front_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot));
                holder.back_dot.setImageDrawable(context.getResources().getDrawable(R.drawable.dot_selected));
                ArrayList<String> images = new ArrayList<>();
                for (int i = 0; i < inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().size(); i++) {
                    if (inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumenttype() == 4) {
                        images.add(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getRci().get(i).getDocumentlocation());
                    }
                    else {
                    }
                }
                holder.imagesLayout.removeAllViews();
                for (int i = 0; i < images.size(); i++) {
                    View v = inflater.inflate(R.layout.list_image, null);
                    ImageView imageView = v.findViewById(R.id.image);
                    Glide.with(context).load(Constants.IMAGE_URL+images.get(i)).into(imageView);
                    holder.imagesLayout.addView(v);
                }
            }
        });

        if (inboxArrayList.get(position).getRequeststatus().equals("Pending")){

            holder.mbids_count_layout.setVisibility(View.VISIBLE);
            holder.expiresInLayout.setVisibility(View.VISIBLE);
            holder.mprice_layout.setVisibility(View.GONE);
            holder.button.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")){
                holder.numberofbid.setText("Number of bids");
            }
            else {
                holder.numberofbid.setText("لا يوجد عروض أسعار");
            }

            try {
                long endMillis = 0;
                String remainingTime = inboxArrayList.get(position).getRemainingTime();
                if (remainingTime.contains("-")) {
                    endMillis = 0;
                }
                else {
                    remainingTime = remainingTime.replace(".0", "");
                    endMillis = Long.parseLong(remainingTime);
                    endMillis = endMillis * 60 * 1000;
                }
                long remainingMillis = endMillis;
                String hms = "00h 00m";
                if (remainingMillis > (60 * 1440 * 1000)) {
                    hms = String.format("%02dd %02dh", TimeUnit.MILLISECONDS.toDays(remainingMillis),
                            TimeUnit.MILLISECONDS.toHours(remainingMillis) % TimeUnit.DAYS.toHours(1));
                }
                else {
                    hms = String.format("%02dh %02dm", TimeUnit.MILLISECONDS.toHours(remainingMillis),
                            TimeUnit.MILLISECONDS.toMinutes(remainingMillis) % TimeUnit.HOURS.toMinutes(1));
                }
                if (endMillis <= 0) {
                    holder.expiresIn.setText("00h 00m");
                } else {
                    holder.expiresIn.setText(Constants.convertToArabic(hms));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.numberofbid.setTextColor(Color.parseColor("#000000"));

            holder.bids_count.setText(""+inboxArrayList.get(position).getRequestcount());

            if(inboxArrayList.get(position).getRequestcount() > 0){
                holder.titles.setVisibility(View.VISIBLE);
                holder.bidsLayout.setVisibility(View.VISIBLE);
                holder.dropdown.setVisibility(View.VISIBLE);
                holder.bidsLayout.removeAllViews();
                for (int i = 0; i < inboxArrayList.get(position).getBidlist().size(); i++) {
//                    LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                    View v;
                    if (language.equalsIgnoreCase("En")){
                        v = inflater.inflate(R.layout.receiving_child, null);
                    }
                    else {
                        v = inflater.inflate(R.layout.receiving_child_ar, null);
                    }

                    TextView requestCode = (TextView) v.findViewById(R.id.request_code);
                    TextView distance = (TextView) v.findViewById(R.id.distance);
                    TextView rating = (TextView) v.findViewById(R.id.rating);
                    TextView minQuote = (TextView) v.findViewById(R.id.minumum_quote);
                    TextView maxQuote = (TextView) v.findViewById(R.id.maxumum_quote);
                    TextView sar = (TextView) v.findViewById(R.id.sar);
                    TextView sar1 = (TextView) v.findViewById(R.id.sar1);
                    RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingbar);

                    if (language.equalsIgnoreCase("En")){
                        requestCode.setText(""+inboxArrayList.get(position).getBidlist().get(i).getWks().get(0).getWorkshopnameEn());
                    }
                    else {
                        requestCode.setText(""+inboxArrayList.get(position).getBidlist().get(i).getWks().get(0).getWorkshopnameAr());
                        requestCode.setTypeface(Constants.getarTypeFace(context));
                        sar.setTypeface(Constants.getarTypeFace(context));
                        sar1.setTypeface(Constants.getarTypeFace(context));
                    }
                    minQuote.setText(Constants.convertToArabic(Constants.priceFormat.format(inboxArrayList.get(position).getBidlist().get(i).getMinquote())));
                    maxQuote.setText(Constants.convertToArabic(Constants.priceFormat.format(inboxArrayList.get(position).getBidlist().get(i).getMaxquote())));
                    distance.setText(""+inboxArrayList.get(position).getBidlist().get(i).getWks().get(0).getWorkshopdistance() + "KM");

                    rating.setText(inboxArrayList.get(position).getBidlist().get(i).getWks().get(0).getAveragerating()+"/5");
                    ratingBar.setRating(inboxArrayList.get(position).getBidlist().get(i).getWks().get(0).getAveragerating());

                    final int finalI = i;
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, OfferDetailsActivity.class);
                            intent.putExtra("id", ""+inboxArrayList.get(position).getRequestid());
                            intent.putExtra("carData", inboxArrayList);
                            intent.putExtra("carPos", position);
                            intent.putExtra("bidPos", finalI);
                            context.startActivity(intent);
                        }
                    });
                    holder.bidsLayout.addView(v);
                }
            }
            else {
                holder.bidsLayout.setVisibility(View.GONE);
                holder.titles.setVisibility(View.GONE);
                holder.dropdown.setVisibility(View.INVISIBLE);
            }

        } else if (inboxArrayList.get(position).getRequeststatus().equals("Accepted")) {

            holder.bidsLayout.setVisibility(View.GONE);
            holder.titles.setVisibility(View.GONE);
            holder.dropdown.setVisibility(View.INVISIBLE);
            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.expiresInLayout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")){
            holder.button.setText("Pending final price");
            holder.numberofbid.setText("Number of bids");
            holder.final_price.setText("Pending");
            }
            else {
                holder.button.setText("انتظار عرض السعر النهائي");
                holder.numberofbid.setText("عرض جديد");
                holder.final_price.setText("قيد الانتظار");
            }
            holder.numberofbid.setTextColor(Color.parseColor("#000000"));

            holder.min_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote())));
            holder.max_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote())));


        } else if (inboxArrayList.get(position).getRequeststatus().equals("Accepted Final Price")) {

            holder.bidsLayout.setVisibility(View.GONE);
            holder.titles.setVisibility(View.GONE);
            holder.dropdown.setVisibility(View.INVISIBLE);
            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.expiresInLayout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")){

                holder.button.setText("View details");
                holder.numberofbid.setText("Number of bids");
            }
            else {
                holder.button.setText("اظهار التفاصيل");
                holder.numberofbid.setText("عرض جديد");
            }
            holder.numberofbid.setTextColor(Color.parseColor("#000000"));

            holder.min_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote())));
            holder.max_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote())));
            holder.final_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice())));

        } else if (inboxArrayList.get(position).getRequeststatus().equals("Pending Accept Final Price")) {

            holder.bidsLayout.setVisibility(View.GONE);

            holder.titles.setVisibility(View.GONE);
            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.expiresInLayout.setVisibility(View.GONE);
            holder.mprice_layout.setVisibility(View.VISIBLE);
            holder.button.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {

                holder.button.setText("Accept final price");
                holder.numberofbid.setText("Number of bids");
            }
            else {

                holder.button.setText("قبول عرض السعر النهائي ");
                holder.numberofbid.setText("عرض جديد");
            }
            holder.numberofbid.setTextColor(Color.parseColor("#000000"));

            holder.min_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMinquote())));
            holder.max_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getMaxquote())));
            holder.final_price.setText(Constants.convertToArabic(Constants.priceFormat1.format(inboxArrayList.get(position).getCm().get(0).getMdl().get(0).getRb().get(0).getFinalprice())));
        }
        else if (inboxArrayList.get(position).getRequeststatus().equalsIgnoreCase("expired")){
            holder.mbids_count_layout.setVisibility(View.GONE);
            holder.expiresInLayout.setVisibility(View.VISIBLE);
            holder.mprice_layout.setVisibility(View.GONE);
            holder.button.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")){
                holder.expiresInTitle.setText("Request expired");
            }
            else {
                holder.expiresInTitle.setText("انتهى وقت الطلب");
            }

            holder.expiresInTitle.setTextColor(Color.parseColor("#FF0000"));
            holder.bids_count.setText("");
            holder.expiresIn.setText("");

            holder.bidsLayout.setVisibility(View.GONE);
            holder.titles.setVisibility(View.GONE);
            holder.dropdown.setVisibility(View.INVISIBLE);
        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inboxArrayList.get(position).getRequeststatus().equalsIgnoreCase("pending")){
                    Intent intent = new Intent(context, OffersActivity.class);
                    intent.putExtra("id", ""+inboxArrayList.get(position).getRequestid());
                    intent.putExtra("carData", inboxArrayList);
                    intent.putExtra("carPos",position);
                    context.startActivity(intent);
                }
                else if(inboxArrayList.get(position).getRequeststatus().equalsIgnoreCase("Accepted Final Price")){
                    Intent intent = new Intent(context, FinalPrice_Bid.class);
                    intent.putExtra("data", inboxArrayList);
                    intent.putExtra("bidPos",position);
                    context.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(context, FinalPrice_Bid.class);
                    intent.putExtra("data", inboxArrayList);
                    intent.putExtra("bidPos",position);
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }
}

